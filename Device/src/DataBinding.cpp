/*
 * DataBinding.cpp
 *
 *  Created on: May 10, 2015
 *      Author: pnikiel
 */

#include <DataBinding.h>
#include <iostream>
#include <stdexcept>
namespace Device
{

//! This may suggest multiple variables but in fact it operates on just one
UaStatus fromSnmpToUaVariant( const netsnmp_variable_list * var, UaVariant & v, const UaString & type)
{
	// what if the pointer is empty? -- since val is an union of pointers, any field can be checked
	if (! var->val.string)
		return OpcUa_Bad;

	if (type == "string")
	{
			std::string s (reinterpret_cast<const char*>(var->val.string), var->val_len);
			v.setString( UaString( s.c_str()  ) );
			return OpcUa_Good;
	}
	else if (type == "int")
	{
		v.setInt32( *var->val.integer );
		return OpcUa_Good;
	}
	else
	{
		std::cout << "type is unknown=" << type.toUtf8() << std::endl;
		return OpcUa_Bad;
	}

// old code: cannot work because ASN.1 type cannot always be automatically understood; e.g. when application profile is used instead of universal profile
//	switch (var->type)
//	{
//	case ASN_OCTET_STR:
//	{
//		std::string s (reinterpret_cast<const char*>(var->val.string), var->val_len);
//		v.setString( UaString( s.c_str()  ) );
//		return OpcUa_Good;
//	}
//	case ASN_BOOLEAN:
//	{
//		throw std::runtime_error("not yet supported");
//	}
//	case ASN_INTEGER:
//	{
//		v.setInt32( *var->val.integer );
//	}
//
//	default:
//		std::cout << "type not implemented: " << var->type << std::endl;
//		return OpcUa_Bad;
//	}
}



}


