#!/bin/sh
DETAIL=$1

if [ "$DETAIL" == "" ]; then
	DETAIL=0
fi

echo "Chosen detail level: $DETAIL (if you want another just put a digit as first param, supported: 0 1)"
java -jar saxon9he.jar Design.xml designToDot.xslt -o:Design.dot detailLevel=$DETAIL 
dot -Tpdf -odiagram.pdf Design.dot

