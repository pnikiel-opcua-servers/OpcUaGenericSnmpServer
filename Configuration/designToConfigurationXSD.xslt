<?xml version="1.0" encoding="UTF-8"?>
<!-- Authors:      Piotr Nikiel <piotr@nikiel.info> -->
<!-- Created on:   14 Jul 2014                      -->
<!-- (C) CERN 2014                                  -->
<xsl:transform version="2.0" xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:d="http://www.example.org/Design"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:output indent="yes" method="xml"></xsl:output>
	<xsl:template name="dataTypeToXsdType">
	<xsl:param name="dataType"/>
	
	<xsl:choose>
	<xsl:when test="$dataType='UaString'">string</xsl:when>
	<xsl:when test="$dataType='OpcUa_Int32'">int</xsl:when>
	<xsl:when test="$dataType='OpcUa_UInt32'">unsignedInt</xsl:when>
	<xsl:when test="$dataType='OpcUa_Int64'">long</xsl:when>
	<xsl:when test="$dataType='OpcUa_UInt64'">unsignedLong</xsl:when>
	<xsl:when test="$dataType='OpcUa_Boolean'">boolean</xsl:when>
	<xsl:when test="$dataType='OpcUa_Double'">double</xsl:when>
	<xsl:when test="$dataType='OpcUa_Float'">float</xsl:when>
	<xsl:when test="$dataType='UaVariant'"><xsl:message terminate="yes">ERROR: it is not allowed to initialize a variable of variable type (UaVariant) from configuration.</xsl:message></xsl:when>
	<xsl:otherwise><xsl:message terminate="yes">ERROR: unknown type <xsl:value-of select="$dataType"/></xsl:message></xsl:otherwise>
	</xsl:choose>
	
	</xsl:template>

	<xsl:template match="d:class">	
	<!--  for every class we create a complexType -->
	<xsl:element name="xs:complexType">
	<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>

	<xs:sequence>
	<xs:choice minOccurs="0" maxOccurs="unbounded">
		<xs:element name="item" type="tns:ITEM" >
		</xs:element>
		<xs:element name="regexpr" type="tns:REGEXPR" >
		</xs:element>
	

		<xsl:for-each select="d:hasobjects[@instantiateUsing='configuration']">
		<xs:element name="{@class}" type="tns:{@class}"  />
		</xsl:for-each>


	</xs:choice>
	</xs:sequence>
	<!-- name of the object of this class is mandatory and not configurable. -->
	<xs:attribute name="name" type="string" use="required"/>
	<!-- now go over all cache-variables that shall be initialized from config file -->
	
	<xsl:for-each select="child::d:cachevariable">
		<xsl:if test="@initializeWith='configuration'">
			<!--  <xs:attribute name="{@name}" type="{@xsdType}" use="required"/>-->
			<xsl:element name="xs:attribute">
				<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
				<xsl:attribute name="use">required</xsl:attribute>
				<xsl:attribute name="type">
				<xsl:call-template name="dataTypeToXsdType">
				<xsl:with-param name="dataType" select="@dataType"/>
				</xsl:call-template>
				</xsl:attribute>
			</xsl:element>
		</xsl:if>
	</xsl:for-each>

	<!-- also config entries shall create attributes in xsd -->
	<xsl:for-each select="d:configentry">
		<xsl:element name="xs:attribute">
			<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
			<xsl:attribute name="use">required</xsl:attribute>
			<xsl:attribute name="type">
			<xsl:call-template name="dataTypeToXsdType">
			<xsl:with-param name="dataType" select="@dataType"/>
			</xsl:call-template>
			</xsl:attribute>
		</xsl:element>
	</xsl:for-each>	

	
	</xsl:element>
	

	
	
	</xsl:template>

	
	<xsl:template match="/">
	<schema xmlns="http://www.w3.org/2001/XMLSchema" targetNamespace="http://www.example.org/Configuration" xmlns:tns="http://www.example.org/Configuration" elementFormDefault="qualified">
	<xs:complexType name="REGEXPR">
		<attribute name="name" type="string" use="required" />
		<attribute name="value" type="string" use="required" />
	</xs:complexType>

	<xs:complexType name="ITEM">
		<attribute name="name" type="string" use="required" />
		<attribute name="value" type="string" use="required" />
		<attribute name="when" type="string" use="optional" />
		<attribute name="status" type="string" use="optional" />
	</xs:complexType>	
	<xsl:apply-templates/>
	
	<xs:complexType name="Configuration">
		<xs:sequence>
			<xs:choice minOccurs="0" maxOccurs="unbounded"  >
				<xsl:for-each select="/d:design/d:root/d:hasobjects[@instantiateUsing='configuration']">
				<xs:element name="{@class}" type="tns:{@class}" />
				</xsl:for-each>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>	
	
	<xs:element name="configuration" type="tns:Configuration" />
	
	</schema>
	</xsl:template>

</xsl:transform>
