/*
 * ASUtils.h
 *
 *  Created on: Jun 12, 2014
 *      Author: pnikiel
 */

#ifndef ASUTILS_H_
#define ASUTILS_H_

#include <uanodeid.h>
#include <uabasenodes.h>
#include <stdlib.h>
#include <iostream>
#include <string>

void abort_with_message(const char* file, int line, const char* message);


#define ASSERT_GOOD(status) { if (!(status).isGood()) { abort_with_message(__FILE__, __LINE__, status.toString().toUtf8()); } }

#define ABORT_MESSAGE(message) { abort_with_message(__FILE__, __LINE__, message); }
#define CONCAT2(s1,s2) ((" " +std::string(s1)+" "+std::string(s2)+" ").c_str())
#define CONCAT3(s1,s2,s3) (CONCAT2(s1,CONCAT2(s2,s3)))

#define PRINT(t) std::cout << t << std::endl;
#define PRINT_LOCATION() std::cout << __PRETTY_FUNCTION__ << std::endl

#endif /* ASUTILS_H_ */
