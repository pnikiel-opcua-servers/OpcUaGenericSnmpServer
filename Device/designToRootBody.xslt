<?xml version="1.0" encoding="UTF-8"?>
<!-- Authors:      Piotr Nikiel <piotr@nikiel.info> -->
<!-- Created on:   18 Jul 2014                      -->
<!-- (C) CERN  Generic OPC UA Server Framework      -->
<xsl:transform version="2.0" xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:d="http://www.example.org/Design"
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:include href="../Design/CommonFunctions.xslt" />
	<xsl:output method="text"></xsl:output>
	 
	
	 	<xsl:template match="/">
	 	#include &lt;iostream&gt;
	 	#include &lt;boost/foreach.hpp&gt;
		#include &lt;DRoot.h&gt;
		#include &lt;ASUtils.h&gt;
		<xsl:for-each select="/d:design/d:root/d:hasobjects">
		<xsl:variable name="className"><xsl:value-of select="@class"/></xsl:variable>
		<xsl:if test="/d:design/d:class[@name=$className]/@deviceLogicTypeName">
		#include &lt;<xsl:value-of select="fnc:DClassName(@class)"/>.h&gt;
		</xsl:if>
		</xsl:for-each>
		namespace Device
		{
		
		DRoot::DRoot() 
		{
			if (m_instance != 0)
				abort(); /* Logic error inside program. DRoot can be instantiated just once */
			m_instance = this;
		}

		DRoot::~DRoot()
		{
			<xsl:for-each select="/d:design/d:root/d:hasobjects">
			<xsl:variable name="className"><xsl:value-of select="@class"/></xsl:variable>
			<xsl:if test="/d:design/d:class[@name=$className]/@deviceLogicTypeName">
			for (auto it = m_<xsl:value-of select="fnc:DClassName(@class)"/>s.begin(); it!=m_<xsl:value-of select="fnc:DClassName(@class)"/>s.end();it++)
				delete (*it);
			m_<xsl:value-of select="fnc:DClassName(@class)"/>s.clear();
			</xsl:if>
			</xsl:for-each>
		}

		DRoot* DRoot::getInstance()
		{
			if (m_instance == 0)
			{
				ABORT_MESSAGE ("DRoot: instance not yet created. ");
			}
			return m_instance;
		}

		void DRoot::unlinkAllChildren () const
		{
			unsigned int objectCounter = 0;
			<xsl:for-each select="/d:design/d:root/d:hasobjects">
			<xsl:variable name="className"><xsl:value-of select="@class"/></xsl:variable>
			<xsl:if test="/d:design/d:class[@name=$className]/@deviceLogicTypeName">
			for (auto it = m_<xsl:value-of select="fnc:DClassName(@class)"/>s.begin(); it!=m_<xsl:value-of select="fnc:DClassName(@class)"/>s.end();it++)
				objectCounter += (*it)->unlinkAllChildren();
			</xsl:if>
			</xsl:for-each>
			std::cout &lt;&lt; "DRoot::unlinkAllChildren(): " &lt;&lt; objectCounter &lt;&lt; " objects unlinked " &lt;&lt; std::endl;
		}
		
		/* find methods for children */
		<xsl:for-each select="/d:design/d:root/d:hasobjects">
		<xsl:variable name="class"><xsl:value-of select="@class"/></xsl:variable>
		<xsl:for-each select="/d:design/d:class[@name=$class]/d:cachevariable[@isKey='true']">
		
		<xsl:value-of select="fnc:DClassName($class)"/> * DRoot::get<xsl:value-of select="$class"/>By<xsl:value-of select="fnc:capFirst(@name)"/> (
		<xsl:choose>
		<xsl:when test="@dataType='UaString'">std::string </xsl:when>
		<xsl:otherwise><xsl:value-of select="@dataType"/></xsl:otherwise>
		</xsl:choose>
		key) const
		{
			BOOST_FOREACH( <xsl:value-of select="fnc:DClassName($class)"/> *test, m_<xsl:value-of select="fnc:DClassName($class)"/>s )
			{
				if (test-&gt;<xsl:value-of select="@name"/>() == key)
					return test;
			}
			return 0;
		}
		</xsl:for-each>
		</xsl:for-each>
		
		/* Singleton's instance. */
		DRoot* DRoot::m_instance = 0;
		
		
	 	}
	 	

	</xsl:template>
	 
	 
	 	 
	 
</xsl:transform>
