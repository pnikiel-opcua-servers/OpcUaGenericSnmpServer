#!/bin/sh

source ../Design/functions.sh

OUTPUT=src/D$1.cpp


transformDesign designToDeviceBody.xslt $OUTPUT 1 1 className=$1
if [ $? -ne 0 ]; then 
	exit 1 
fi
