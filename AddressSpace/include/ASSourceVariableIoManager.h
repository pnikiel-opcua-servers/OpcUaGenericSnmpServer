/*
 * ASSourceVariableIoManager.h
 *
 *  Created on: Sep 16, 2014
 *      Author: pnikiel
 */

#ifndef ASSOURCEVARIABLEIOMANAGER_H_
#define ASSOURCEVARIABLEIOMANAGER_H_

#include <iomanager.h>
#include <SourceVariables.h>

namespace AddressSpace
{

class ASSourceVariableIoManager: public IOManager

{
public:

	ASSourceVariableIoManager (ASSourceVariableJobId readOp, ASSourceVariableJobId writeOp, const UaNode* variableParentNode):
		m_readOperationJobId (readOp),
		m_writeOperationJobId (writeOp),
		m_variableParentNode (variableParentNode),
		m_callback(0),
		m_transaction(0),
		m_callbackHandle(0)
{}
	virtual ~ASSourceVariableIoManager () {}

    virtual UaStatus beginTransaction (
        IOManagerCallback*       pCallback,
        const ServiceContext&    serviceContext,
        OpcUa_UInt32             hTransaction,
        OpcUa_UInt32             totalItemCountHint,
        OpcUa_Double             maxAge,
        OpcUa_TimestampsToReturn timestampsToReturn,
        TransactionType          transactionType,
        OpcUa_Handle&            hIOManagerContext);

    virtual UaStatus beginStartMonitoring(
        OpcUa_Handle        hIOManagerContext,
        OpcUa_UInt32        callbackHandle,
        IOVariableCallback* pIOVariableCallback,
        VariableHandle*     pVariableHandle,
        MonitoringContext&  monitoringContext)
    {
    	return OpcUa_Bad;
    }

    virtual UaStatus beginModifyMonitoring(
        OpcUa_Handle        hIOManagerContext,
        OpcUa_UInt32        callbackHandle,
        OpcUa_UInt32        hIOVariable,
        MonitoringContext&  monitoringContext)
    {
    	return OpcUa_Bad;
    }

    virtual UaStatus beginStopMonitoring(
        OpcUa_Handle        hIOManagerContext,
        OpcUa_UInt32        callbackHandle,
        OpcUa_UInt32        hIOVariable)
    {
    	return OpcUa_Bad;
    }

    virtual UaStatus beginRead (
        OpcUa_Handle        hIOManagerContext,
        OpcUa_UInt32        callbackHandle,
        VariableHandle*     pVariableHandle,
        OpcUa_ReadValueId*  pReadValueId) ;

    virtual UaStatus beginWrite (
        OpcUa_Handle        hIOManagerContext,
        OpcUa_UInt32        callbackHandle,
        VariableHandle*     pVariableHandle,
        OpcUa_WriteValue*   pWriteValue) ;

    virtual UaStatus finishTransaction (
        OpcUa_Handle        hIOManagerContext);

private:
    ASSourceVariableJobId m_readOperationJobId;
    ASSourceVariableJobId m_writeOperationJobId;
    const UaNode * m_variableParentNode;
	IOManagerCallback*  m_callback;
	OpcUa_UInt32 m_transaction;
	OpcUa_UInt32 m_callbackHandle;

};


}




#endif /* ASSOURCEVARIABLEIOMANAGER_H_ */
