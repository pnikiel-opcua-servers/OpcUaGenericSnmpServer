#!/bin/sh

if [ ! -f "Device/include/DRoot.h" ] && [ ! -f "Device/src/DRoot.cpp" ]; then
	cd Device
	./generateRoot.sh
	cd ..
fi

./generateCmake.sh $1
make -j `nproc`

