#!/bin/sh

source ../Design/functions.sh

OUTPUT=src/AS$1.cpp

transformDesign designToClassBody.xslt $OUTPUT 0 1 className=$1
if [ $? -ne 0 ]; then 
	exit 1 
fi
