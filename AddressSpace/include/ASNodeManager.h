/*
 * CASNodeManager.h
 *
 *  Created on: Jun 20, 2014
 *      Author: pnikiel <piotr@nikiel.info>
 *      Based on work of Viatcheslav Filimonov <vfilimon@cern.ch>
 */

#ifndef CASNODEMANAGER_H_
#define CASNODEMANAGER_H_

#include <iostream>
#include <boost/xpressive/xpressive.hpp>

#include <nodemanagerbase.h>

#include <ASUtils.h>


namespace AddressSpace

{

class ASNodeManager : public NodeManagerBase
{
	UA_DISABLE_COPY(ASNodeManager);
public:
	ASNodeManager();
	virtual ~ASNodeManager();

	virtual UaStatus afterStartUp();
	virtual UaStatus beforeShutDown();

	UaObject * getInstanceDeclarationObjectType (OpcUa_UInt32 typeId);

    virtual IOManager* getIOManager(UaNode* pUaNode, OpcUa_Int32 attributeId) const;

/*
	// improve this
	OpcUa_Int32  findUaVariables(OpcUa::BaseObjectType *pParObj,std::string& pattern,UaControlVariableSet& storage);
	OpcUa_Int32  findUaNodeVariables(UaNode *pNode,std::string & pattern,UaControlVariableSet& storage);
	*/

//	/* get rid of this */
//	UaControlVariable * findUaControlVariable (std::string & fullName);

	UaNodeId makeChildNodeId (const UaNodeId &parent, const UaString& childName);


	template<typename T>
	T* findByStringId (const std::string & stringId)
	{
		UaNode* node = getNode (UaNodeId (stringId.c_str(), getNameSpaceIndex()));
		if (node && node == dynamic_cast<T*>(node) )
		{
			/* Alright, we deal with given type or its descendant */
			return (T*) node;
		}
		else
			return 0;
	}



	template<typename T>
	unsigned int findVariablesByRegex (UaNode* startNode, const boost::xpressive::sregex & expression, std::vector<T*> &storage)
	{

		UaNodeId id = startNode->nodeId();
		if (startNode->nodeClass() == OpcUa_NodeClass_Variable)
		{
			if (id.identifierType() == OpcUa_IdentifierType_String)
			{

				std::string sId = UaString(id.identifierString()).toUtf8();


				boost::xpressive::smatch what;
				if (boost::xpressive::regex_match( sId , what, expression ))
				{
					std::cout << "name matched:" << sId << std::endl;
					T* t = dynamic_cast<T*>(startNode);
					if (t == startNode) /* this type itself or its descendant */
					{
						std::cout << "type also matched" << std::endl;
						storage.push_back( t );
						return 1;
					}
					else
						return 0; /* type mismatch */
				}
				else
					return 0; /* name mismatch */
			}
			else
				return 0; /* strange identifier type */
		}
		else if (startNode->nodeClass() == OpcUa_NodeClass_Object)
		{
			unsigned int nMatched=0;
			UaReference *pRefList = const_cast<UaReference *>(startNode->getUaReferenceLists()->pTargetNodes());
			while (pRefList)
			{
				nMatched = nMatched + findVariablesByRegex( pRefList->pTargetNode(), expression, storage);
				pRefList = pRefList->pNextForwardReference();
			}
			return nMatched;
		}
		else
			return 0;
	}

	template<typename T>
	unsigned int findVariablesByPattern (UaNode* startNode, const std::string & pattern, std::vector<T*> &storage)
	{
		boost::xpressive::sregex expression;
		try
		{
			 expression = boost::xpressive::sregex::compile( pattern );
		}
		catch (boost::xpressive::regex_error &e)
		{
			ABORT_MESSAGE(CONCAT3(" REGEX Expression is wrong:",pattern,e.what()));
		}
		return findVariablesByRegex<T> (startNode, expression, storage);
	}

	template<typename T>
	unsigned int findAllByRegex (UaNode* startNode, OpcUa_NodeClass nodeClass, const boost::xpressive::sregex & expression, std::vector<T*> &storage)
	{

		UaNodeId id = startNode->nodeId();
		unsigned int numAdded=0;
		if (startNode->nodeClass() == nodeClass)
		{
			if (id.identifierType() == OpcUa_IdentifierType_String)
			{
				std::string sId = UaString(id.identifierString()).toUtf8();
				boost::xpressive::smatch what;
				if (boost::xpressive::regex_match( sId , what, expression ))
				{
					//std::cout << "name matched:" << sId << std::endl;
					T* t = dynamic_cast<T*>(startNode);
					if (t == startNode) /* this type itself or its descendant */
					{
					//	std::cout << "type also matched" << std::endl;
						storage.push_back( t );
						numAdded++;
					}
					//else
					//	std::cout << "type didnt match:" << typeid(startNode).name() << std::endl;
				}
			}
		}
		if (startNode->nodeClass() == OpcUa_NodeClass_Object)
		{
			unsigned int nMatched=0;
			UaReference *pRefList = const_cast<UaReference *>(startNode->getUaReferenceLists()->pTargetNodes());
			while (pRefList)
			{
				nMatched = nMatched + findAllByRegex( pRefList->pTargetNode(), nodeClass, expression, storage);
				pRefList = pRefList->pNextForwardReference();
			}
			return nMatched;
		}
		else
			return numAdded;
	}

	template<typename T>
	unsigned int findAllByPattern (UaNode* startNode, OpcUa_NodeClass nodeClass, const std::string & pattern, std::vector<T*> &storage)
	{
		boost::xpressive::sregex expression;
		try
		{
			 expression = boost::xpressive::sregex::compile( pattern );
		}
		catch (boost::xpressive::regex_error &e)
		{
			ABORT_MESSAGE(CONCAT3(" REGEX Expression is wrong:",pattern,e.what()));
		}
		return findAllByRegex<T> (startNode, nodeClass, expression, storage);
	}

	const UaNodeId getTypeNodeId (unsigned int numericalType);


private:
	UaStatus createTypeNodes();
};





}



#endif /* CASNODEMANAGER_H_ */
