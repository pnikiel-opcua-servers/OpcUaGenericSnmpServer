<?xml version="1.0" encoding="UTF-8"?>
<!-- Authors:      Piotr Nikiel <piotr@nikiel.info> -->
<!-- Created on:   14 Jul 2014                      -->
<!-- (C) CERN 2014                                  -->
<xsl:transform version="2.0" xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:d="http://www.example.org/Design"
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:include href="../Design/CommonFunctions.xslt" />
	<xsl:output method="text"/>
	<xsl:param name="className"/>
	<xsl:strip-space elements="*"/>



	<xsl:template name="deviceBody">
	 
		<xsl:if test="@deviceLogicTypeName">
		void <xsl:value-of select="fnc:Base_DClassName(@name)"/>::linkAddressSpace( AddressSpace::<xsl:value-of select="fnc:ASClassName(@name)"/> *addressSpaceLink)
		{
			if (m_addressSpaceLink != 0)
			{
				/* signalize nice error from here. */
				abort();
			}
			else
				m_addressSpaceLink = addressSpaceLink;	
		}
		
		/* add/remove */
		<xsl:for-each select="d:hasobjects">
		void <xsl:value-of select="fnc:Base_DClassName($className)"/>::add ( <xsl:value-of select="fnc:DClassName(@class)"/> *device)
		{
			m_<xsl:value-of select="@class"/>s.push_back (device);
		}
		</xsl:for-each>
		
		//! Disconnects AddressSpace part from the Device logic, and does the same for all children
		//! Returns number of unlinked objects including self
		unsigned int <xsl:value-of select="fnc:Base_DClassName($className)"/>::unlinkAllChildren ()
		{
			unsigned int objectCounter=1;  // 1 is for self
			m_addressSpaceLink = 0;
			/* Fill up: call unlinkAllChildren on all children */
			<xsl:for-each select="d:hasobjects">
			<xsl:variable name="childClassName"><xsl:value-of select="@class"/></xsl:variable>
			<xsl:if test="/d:design/d:class[@name=$childClassName]/@deviceLogicTypeName">
			BOOST_FOREACH( <xsl:value-of select="fnc:DClassName(@class)"/> *d, m_<xsl:value-of select="@class"/>s )
			{
				objectCounter += d->unlinkAllChildren();
			}
			</xsl:if>
			</xsl:for-each>
			return objectCounter;
			
		}
		
		/* find methods for children */
		<xsl:for-each select="d:hasobjects">
		<xsl:variable name="class"><xsl:value-of select="@class"/></xsl:variable>
		<xsl:for-each select="/d:design/d:class[@name=$class]/d:cachevariable[@isKey='true']">
		
		<xsl:value-of select="fnc:DClassName($class)"/> * <xsl:value-of select="fnc:Base_DClassName($className)"/>::get<xsl:value-of select="$class"/>By<xsl:value-of select="fnc:capFirst(@name)"/> (<xsl:value-of select="@dataType"/> key) const
		{
			BOOST_FOREACH( <xsl:value-of select="fnc:DClassName($class)"/> *test, m_<xsl:value-of select="$class"/>s )
			{
				if (test-&gt;<xsl:value-of select="@name"/>() == key)
					return test;
			}
			return 0;
		}
		</xsl:for-each>
		
		<xsl:for-each select="/d:design/d:class[@name=$class]/d:configentry[@isKey='true']">
		
		<xsl:value-of select="fnc:DClassName($class)"/> * <xsl:value-of select="fnc:Base_DClassName($className)"/>::get<xsl:value-of select="$class"/>By<xsl:value-of select="fnc:capFirst(@name)"/> (<xsl:value-of select="@dataType"/> key) const
		{
			BOOST_FOREACH( <xsl:value-of select="fnc:DClassName($class)"/> *test, m_<xsl:value-of select="$class"/>s )
			{
				if (test-&gt;<xsl:value-of select="@name"/>() == key)
					return test;
			}
			return 0;
		}
		
		</xsl:for-each><!-- for-each configEntry -->
		
		<xsl:if test="fnc:isHasObjectsSingleton(.)='true'">
		// This function is generated because the hasObjects links to exactly 1 object
		<xsl:value-of select="fnc:DClassName(@class)"/> * <xsl:value-of select="fnc:Base_DClassName($className)"/>::<xsl:value-of select="lower-case(@class)"/>() const
		{
		  if (m_<xsl:value-of select="@class"/>s.size() != 1)
		    throw std::runtime_error( "Configuration error: <xsl:value-of select="$className"/> should have exactly 1 <xsl:value-of select="@class"/> " );
		  return m_<xsl:value-of select="@class"/>s.at(0);
		}
		</xsl:if>
		</xsl:for-each><!-- for-each hasObjects -->
		
		</xsl:if>
		


    
    /* Constructor */
		<xsl:value-of select="fnc:Base_DClassName(@name)"/>::<xsl:value-of select="fnc:Base_DClassName(@name)"/> (const Configuration::<xsl:value-of select="@name"/> &amp; config
		<xsl:if test="fnc:getCountParentClassesAndRoot(/,$className)=1">
		, <xsl:value-of select="fnc:getParentDeviceClass(/,$className)"/> * parent
		</xsl:if>
		):
			<xsl:if test="fnc:getCountParentClassesAndRoot(/,$className)=1">
		 	m_parent(parent),
			</xsl:if>
			m_addressSpaceLink(0)	
			<xsl:for-each select="d:cachevariable[@isKey='true']">
			, m_<xsl:value-of select="@name"/> ( config.<xsl:value-of select="@name"/>() )
			</xsl:for-each>
			<xsl:for-each select="d:configentry[@isKey='true']">
			, m_<xsl:value-of select="@name"/> ( config.<xsl:value-of select="@name"/>() )
			</xsl:for-each>
		{

		}
		
		<xsl:value-of select="fnc:Base_DClassName($className)" />::~<xsl:value-of select="fnc:Base_DClassName($className)" /> ()
		{
			/* remove children */
			<xsl:for-each select="/d:design/d:class[@name=$className and @deviceLogicTypeName]/d:hasobjects">
			BOOST_FOREACH( <xsl:value-of select="fnc:DClassName(@class)"/> *d, m_<xsl:value-of select="@class"/>s )
			{
				delete d;
			}
			</xsl:for-each>
		}
    
    		std::string <xsl:value-of select="fnc:Base_DClassName($className)" />::getFullName() const
    		{
    		  if (m_addressSpaceLink)
    		  {
    		    return std::string(m_addressSpaceLink-&gt;nodeId().toFullString().toUtf8());
    		  }
    		  else
    		    return "-ObjectNotBoundToAddressSpace-";
    		}
	 
	 </xsl:template>
	 
	
	 	<xsl:template match="/">	
	 	<xsl:if test="not(/d:design/d:class[@name=$className])">
				<xsl:message terminate="yes">Class '<xsl:value-of select="$className"/>' not found. If you are running this through generateDevice*.sh scripts, maybe you forgot to put name of the class as a parameter?</xsl:message>
		</xsl:if>
		<xsl:if test="not(/d:design/d:class[@name=$className]/@deviceLogicTypeName)">
				<xsl:message terminate="yes">Class '<xsl:value-of select="$className"/>' hasnt got device logic</xsl:message>
		</xsl:if>
	 	/* This is device body stub */

		
#include &lt;boost/foreach.hpp&gt;		
		
#include &lt;Configuration.hxx&gt;


#include &lt;<xsl:value-of select="fnc:Base_DClassName($className)"/>.h&gt;
#include &lt;<xsl:value-of select="fnc:ASClassName($className)"/>.h&gt;


	<xsl:for-each select="/d:design/d:class[@name=$className]">
	<xsl:if test="@deviceLogicTypeName">
	<xsl:for-each select="d:hasobjects">
	#include &lt;<xsl:value-of select="fnc:DClassName(@class)"/>.h&gt;
	</xsl:for-each>
	</xsl:if>
	</xsl:for-each>



namespace Device
{

	<xsl:for-each select="/d:design/d:class[@name=$className]">
	<xsl:call-template name="deviceBody"/>
	</xsl:for-each>
	
	
}


	</xsl:template>
	 
	 
	 
	 
	 
	 
</xsl:transform>
