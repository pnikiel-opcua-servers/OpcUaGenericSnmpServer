<?xml version="1.0" encoding="UTF-8"?>
<!-- Authors:      Piotr Nikiel <piotr@nikiel.info> -->
<!-- Created on:   14 Jul 2014                      -->
<!-- (C) CERN 2014                                  -->
<xsl:transform version="2.0"
	xmlns:xml="http://www.w3.org/XML/1998/namespace" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:d="http://www.example.org/Design" xmlns:fnc="http://MyFunctions"
	xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:include href="../Design/CommonFunctions.xslt" />
	<xsl:output method="text"></xsl:output>
	<xsl:param name="className" />

	<xsl:template name="cachevariables_delegates">
		<xsl:if test="@addressSpaceWrite='delegated'">
			/* Note: never directly call this function. */

			
			UaStatus write<xsl:value-of select="fnc:capFirst(@name)"/> ( const <xsl:value-of select="@dataType"/> &amp; v);
		</xsl:if>
	</xsl:template>

	<xsl:template name="sourcevariables_operations">
		<xsl:if test="@addressSpaceRead='asynchronous'">
			/* ASYNCHRONOUS !! */
			UaStatus read<xsl:value-of select="fnc:capFirst(@name)"/> (
			<xsl:value-of select="@dataType"/> &amp;value,
			UaDateTime &amp;sourceTime
			);
		</xsl:if>
		
		<xsl:if test="@addressSpaceWrite='asynchronous'">
			/* ASYNCHRONOUS !! */
			UaStatus write<xsl:value-of select="fnc:capFirst(@name)"/> (
			<xsl:value-of select="@dataType"/> &amp;value
			);
		</xsl:if>
	</xsl:template>

	<xsl:template name="deviceHeader">

		class
		<xsl:value-of select="fnc:DClassName($className)" />
		: public <xsl:value-of select="fnc:Base_DClassName($className)" />
		{

		public:
		/* sample constructor */
		explicit <xsl:value-of select="fnc:DClassName($className)" /> ( const Configuration::<xsl:value-of select="@name"/> &amp; config 
		<xsl:if test="fnc:getCountParentClassesAndRoot(/,$className)=1">
		, <xsl:value-of select="fnc:getParentDeviceClass(/,$className)"/> * parent
		</xsl:if>
		) ;
		/* sample dtr */
		~<xsl:value-of select="fnc:DClassName($className)" /> ();


		

		/* delegators for
		cachevariables and sourcevariables */
		<xsl:for-each select="d:cachevariable">
			<xsl:call-template name="cachevariables_delegates" />
		</xsl:for-each>
		
		<xsl:for-each select="d:sourcevariable">
			<xsl:call-template name="sourcevariables_operations"/>
		</xsl:for-each>
		
		private:
		/* Delete copy constructor and assignment operator */
		<xsl:value-of select="fnc:DClassName($className)" />( const <xsl:value-of select="fnc:DClassName($className)" /> &amp; ) = delete;
		<xsl:value-of select="fnc:DClassName($className)" />&amp; operator=(const <xsl:value-of select="fnc:DClassName($className)" /> &amp;other) = delete;
		
	// ----------------------------------------------------------------------- *
	// -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
	// -     Don't change this comment, otherwise merge tool may be troubled.  *
	// ----------------------------------------------------------------------- *
	
	public:
	
	private:
	
	
	};
	
	
	</xsl:template>


	<xsl:template match="/">
		<xsl:if test="not(/d:design/d:class[@name=$className])">
				<xsl:message terminate="yes">Class '<xsl:value-of select="$className"/>' not found. If you are running this through generateDevice*.sh scripts, maybe you forgot to put name of the class as a parameter?</xsl:message>
		</xsl:if>
		<xsl:if test="not(/d:design/d:class[@name=$className]/@deviceLogicTypeName)">
				<xsl:message terminate="yes">Class '<xsl:value-of select="$className"/>' hasnt got device logic</xsl:message>
		</xsl:if>
		/* This is device header stub */


		#ifndef __<xsl:value-of select="fnc:DClassName($className)" />__H__
		#define __<xsl:value-of select="fnc:DClassName($className)" />__H__

		#include &lt;vector&gt;
		#include &lt;boost/thread/mutex.hpp&gt;

		#include &lt;statuscode.h&gt;
		#include &lt;uadatetime.h&gt;
		#include &lt;session.h&gt;
		
		
		#include &lt;DRoot.h&gt;
		#include &lt;Configuration.hxx&gt;

		#include &lt;<xsl:value-of select="fnc:Base_DClassName($className)" />.h&gt;


		namespace Device
		{
		

		<xsl:for-each select="/d:design/d:class[@name=$className]">
			<xsl:call-template name="deviceHeader" />
		</xsl:for-each>


		}

		#endif // include guard
	</xsl:template>






</xsl:transform>
