#!/bin/sh

source ./functions.sh

# 1st line of validation -- does it matches its schema?
# This allows some basic checks
echo "1st line of check -- XSD conformance"
xmllint --noout --schema Design.xsd Design.xml
if [ "$?" != "0" ]; then
	exit 1
fi

# 2nd line of validation -- including XSLT
echo "2nd line of check -- more advanced checks using XSLT processor"
OUTPUT=validationOutput.removeme
transformDesign designValidation.xslt $OUTPUT 0 0 
if [ "$?" != "0" ]; then
	exit 1
else
	echo "2nd line of check was successful"
fi
