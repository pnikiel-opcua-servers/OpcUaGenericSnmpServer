/*
 * DAgentInternal.h
 *
 *  Created on: May 11, 2015
 *      Author: pnikiel
 */

#ifndef DEVICE_INCLUDE_DAGENTINTERNAL_H_
#define DEVICE_INCLUDE_DAGENTINTERNAL_H_

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

namespace Device
{

struct DAgentInternal
{

	 snmp_session *session;



};

}



#endif /* DEVICE_INCLUDE_DAGENTINTERNAL_H_ */
