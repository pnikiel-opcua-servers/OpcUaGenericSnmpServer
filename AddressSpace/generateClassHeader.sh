#!/bin/sh

source ../Design/functions.sh

OUTPUT=include/AS$1.h

transformDesign designToClassHeader.xslt $OUTPUT 0 1 className=$1
if [ $? -ne 0 ]; then 
	exit 1 
fi
