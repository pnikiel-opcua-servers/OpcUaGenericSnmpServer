/*
 * ASSourceVariableIoManager.cpp
 *
 *  Created on: Sep 16, 2014
 *      Author: pnikiel
 */

#include <ASSourceVariableIoManager.h>
#include <iostream>

using namespace std;

namespace AddressSpace
{

UaStatus ASSourceVariableIoManager::beginTransaction (
    IOManagerCallback*       pCallback,
    const ServiceContext&    serviceContext,
    OpcUa_UInt32             hTransaction,
    OpcUa_UInt32             totalItemCountHint,
    OpcUa_Double             maxAge,
    OpcUa_TimestampsToReturn timestampsToReturn,
    TransactionType          transactionType,
    OpcUa_Handle&            hIOManagerContext)
{
	m_callback = pCallback;
	m_transaction = hTransaction;
	return OpcUa_Good;
}

UaStatus ASSourceVariableIoManager::beginRead (
    OpcUa_Handle        hIOManagerContext,
    OpcUa_UInt32        callbackHandle,
    VariableHandle*     pVariableHandle,
    OpcUa_ReadValueId*  pReadValueId)
{
	m_callbackHandle = callbackHandle;
	cout << "beginRead op=" << m_readOperationJobId << " cbkHandle=" <<callbackHandle << endl;
	if (m_readOperationJobId == ASSOURCEVARIABLE_NOTHING)
		return OpcUa_BadUserAccessDenied;
	else
		return SourceVariables_spawnIoJobRead (
				m_readOperationJobId,
				m_callback,
				m_transaction,
				m_callbackHandle,
				m_variableParentNode
				);

}

UaStatus ASSourceVariableIoManager::beginWrite (
    OpcUa_Handle        hIOManagerContext,
    OpcUa_UInt32        callbackHandle,
    VariableHandle*     pVariableHandle,
    OpcUa_WriteValue*   pWriteValue)
{
	m_callbackHandle = callbackHandle;
	cout << "beginWrite op=" << m_writeOperationJobId << " cbkHandle=" <<callbackHandle << endl;
	if (m_writeOperationJobId == ASSOURCEVARIABLE_NOTHING)
		return OpcUa_BadUserAccessDenied;
	else
		return SourceVariables_spawnIoJobWrite (
				m_writeOperationJobId,
				m_callback,
				m_transaction,
				m_callbackHandle,
				m_variableParentNode,
				pWriteValue
				);
}

UaStatus ASSourceVariableIoManager::finishTransaction (
    OpcUa_Handle        hIOManagerContext)
{
	return OpcUa_Good;
}



}
