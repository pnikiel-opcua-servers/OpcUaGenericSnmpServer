<?xml version="1.0" encoding="UTF-8"?>
<!-- Authors:      Piotr Nikiel <piotr@nikiel.info> -->
<!-- Created on:   14 Jul 2014                      -->
<!-- (C) CERN 2014-2015                                  -->
<xsl:transform version="2.0"
	xmlns:xml="http://www.w3.org/XML/1998/namespace" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:d="http://www.example.org/Design" xmlns:fnc="http://MyFunctions"
	xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:include href="../Design/CommonFunctions.xslt" />
	<xsl:output method="text"></xsl:output>
	<xsl:param name="className" />

	<xsl:template name="deviceHeader">

		class
		<xsl:value-of select="fnc:Base_DClassName($className)" />
		{

		public:
		/* Constructor */
		explicit <xsl:value-of select="fnc:Base_DClassName($className)" /> ( const Configuration::<xsl:value-of select="@name"/> &amp; config 
		<xsl:if test="fnc:getCountParentClassesAndRoot(/,$className)=1">
		, <xsl:value-of select="fnc:getParentDeviceClass(/,$className)"/> * parent
		</xsl:if>
		) ;
		
		private:
		/* No copy constructors or assignment operators */
		<xsl:value-of select="fnc:Base_DClassName($className)" /> (const <xsl:value-of select="fnc:Base_DClassName($className)" /> &amp; other) = delete;
		<xsl:value-of select="fnc:Base_DClassName($className)" /> &amp; operator=(const <xsl:value-of select="fnc:Base_DClassName($className)" /> &amp; other) = delete;
		
		public:
		
		/* sample dtr */
		virtual ~<xsl:value-of select="fnc:Base_DClassName($className)" /> ();

		<xsl:if test="fnc:getCountParentClassesAndRoot(/,$className)=1">
		<xsl:value-of select="fnc:getParentDeviceClass(/,$className)"/> * getParent () const { return m_parent; } 	
		</xsl:if>

		/* add/remove for handling device structure */
		<xsl:for-each select="d:hasobjects">
		void add ( <xsl:value-of select="fnc:DClassName(@class)"/> *);
		const std::vector&lt;<xsl:value-of select="fnc:DClassName(@class)"/> * &gt; &amp; <xsl:value-of select="lower-case(@class)"/>s () const { return m_<xsl:value-of select="@class"/>s; }
		<xsl:if test="fnc:isHasObjectsSingleton(.)='true'">
		<xsl:value-of select="fnc:DClassName(@class)"/> * <xsl:value-of select="lower-case(@class)"/>() const;
		</xsl:if>
		</xsl:for-each>
		
		/* to safely quit */
		unsigned int unlinkAllChildren ();


		void linkAddressSpace (AddressSpace::
		<xsl:value-of select="fnc:ASClassName($className)" />
		* as);
		AddressSpace::<xsl:value-of select="fnc:ASClassName($className)"/> * getAddressSpaceLink () const { return m_addressSpaceLink; }
		
		
		/* find methods for children */
		<xsl:for-each select="d:hasobjects">
		<xsl:variable name="class"><xsl:value-of select="@class"/></xsl:variable>
		<xsl:for-each select="/d:design/d:class[@name=$class]/d:cachevariable[@isKey='true']">
		/* Returns 0 when element not found */
		<xsl:value-of select="fnc:DClassName($class)"/> * get<xsl:value-of select="$class"/>By<xsl:value-of select="fnc:capFirst(@name)"/> (<xsl:value-of select="@dataType"/> key) const;
		
		</xsl:for-each>
		</xsl:for-each>
		
		<xsl:for-each select="d:hasobjects">
		<xsl:variable name="class"><xsl:value-of select="@class"/></xsl:variable>
		<xsl:for-each select="/d:design/d:class[@name=$class]/d:configentry[@isKey='true']">
		/* Returns 0 when element not found */
		<xsl:value-of select="fnc:DClassName($class)"/> * get<xsl:value-of select="$class"/>By<xsl:value-of select="fnc:capFirst(@name)"/> (<xsl:value-of select="@dataType"/> key) const;
		
		</xsl:for-each>
		</xsl:for-each>
		
		/* getters for values which are keys */
		<xsl:for-each select="d:cachevariable[@isKey='true']">
		const 
		<xsl:choose>
		<xsl:when test="@dataType='UaString'">std::string</xsl:when>
		<xsl:otherwise><xsl:value-of select="@dataType"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text> </xsl:text><xsl:value-of select="@name"/> () { return m_<xsl:value-of select="@name"/>; }
		</xsl:for-each>
		<xsl:for-each select="d:configentry[@isKey='true']">
		const 
		<xsl:choose>
		<xsl:when test="@dataType='UaString'">std::string</xsl:when>
		<xsl:otherwise><xsl:value-of select="@dataType"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text> </xsl:text><xsl:value-of select="@name"/> () { return m_<xsl:value-of select="@name"/>; }
		</xsl:for-each>
		
		
		/* mutex operations */
		<xsl:if test="@deviceLogicHasMutex='true'">
		void lock () { m_lock.lock(); }
		void unlock () { m_lock.unlock(); }
		</xsl:if>
		
		/* variable-wise locks */
		<xsl:for-each select="d:sourcevariable">
		<xsl:if test="@addressSpaceWriteUseMutex='of_this_variable' or @addressSpaceReadUseMutex='of_this_variable'">
		void lockVariable_<xsl:value-of select="@name"/> () { m_lockVariable_<xsl:value-of select="@name"/>.lock(); }
		void unlockVariable_<xsl:value-of select="@name"/> () { m_lockVariable_<xsl:value-of select="@name"/>.unlock(); } 
		</xsl:if>
		</xsl:for-each>
		
		<xsl:for-each select="d:sourcevariable">
		<xsl:if test="@addressSpaceWriteUseMutex='of_this_operation'">
		void lockVariableWrite_<xsl:value-of select="@name"/> () { m_lockVariable_write_<xsl:value-of select="@name"/>.lock(); }
		void unlockVariableWrite_<xsl:value-of select="@name"/> () { m_lockVariable_write_<xsl:value-of select="@name"/>.unlock(); } 		
		</xsl:if>
		</xsl:for-each>
		
		<xsl:for-each select="d:sourcevariable">
		<xsl:if test="@addressSpaceReadUseMutex='of_this_operation'">
		void lockVariableRead_<xsl:value-of select="@name"/> () { m_lockVariable_read_<xsl:value-of select="@name"/>.lock(); }
		void unlockVariableRead_<xsl:value-of select="@name"/> () { m_lockVariable_read_<xsl:value-of select="@name"/>.unlock(); } 	
		</xsl:if>
		</xsl:for-each>
		
		/* query address-space for full name (mostly for debug purposes) */
		std::string getFullName() const;

		private:
		<!-- Check if parent is unique (if more than one class has this class as hasObjects, then it is not -->
		
		<xsl:choose>
			<xsl:when test="fnc:getCountParentClassesAndRoot(/,$className)=1">
			<xsl:value-of select="fnc:getParentDeviceClass(/,$className)"/> * m_parent;
			</xsl:when>
			<xsl:otherwise>
				/* Parent link has not been generated, because this class is a child
				of
				<xsl:value-of select="fnc:getCountParentClassesAndRoot(/,$className)" />
				classes (including root).*/
			</xsl:otherwise>
		</xsl:choose>
	
		public:	  // TODO: reconsider this!
		AddressSpace::
		<xsl:value-of select="fnc:ASClassName($className)" />
		*m_addressSpaceLink;

		private:
		<xsl:for-each select="d:hasobjects">
		std::vector &lt; <xsl:value-of select="fnc:DClassName(@class)"/> * &gt; m_<xsl:value-of select="@class"/>s;
		</xsl:for-each>
		/* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */
		<xsl:for-each select="d:cachevariable[@isKey='true']">
		const 
		<xsl:choose>
			<xsl:when test="@dataType='UaString'">std::string </xsl:when>
			<xsl:otherwise><xsl:value-of select="@dataType"/></xsl:otherwise>
		</xsl:choose> m_<xsl:value-of select="@name"/> ;
		</xsl:for-each>
		
		/* if any of our config entries has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */
		<xsl:for-each select="d:configentry[@isKey='true']">
		const 
		<xsl:choose>
			<xsl:when test="@dataType='UaString'">std::string </xsl:when>
			<xsl:otherwise><xsl:value-of select="@dataType"/></xsl:otherwise>
		</xsl:choose> m_<xsl:value-of select="@name"/> ;
		</xsl:for-each>

		/* object-wise lock */
		<xsl:if test="@deviceLogicHasMutex='true'">
		boost::mutex m_lock;
		</xsl:if>
		
		/* variable-wise locks */
		<xsl:for-each select="d:sourcevariable">
		<xsl:if test="@addressSpaceWriteUseMutex='of_this_variable' or @addressSpaceReadUseMutex='of_this_variable'">
		boost::mutex m_lockVariable_<xsl:value-of select="@name"/> ;
		</xsl:if>
		</xsl:for-each>
		
		<xsl:for-each select="d:sourcevariable">
		<xsl:if test="@addressSpaceWriteUseMutex='of_this_operation'">
		boost::mutex m_lockVariable_write_<xsl:value-of select="@name"/> ;
		</xsl:if>
		</xsl:for-each>
		
		<xsl:for-each select="d:sourcevariable">
		<xsl:if test="@addressSpaceReadUseMutex='of_this_operation'">
		boost::mutex m_lockVariable_read_<xsl:value-of select="@name"/> ;
		</xsl:if>
		</xsl:for-each>

		
	
	
	};
	
	
	</xsl:template>


	<xsl:template match="/">
		<xsl:if test="not(/d:design/d:class[@name=$className])">
				<xsl:message terminate="yes">Class '<xsl:value-of select="$className"/>' not found. If you are running this through generateDevice*.sh scripts, maybe you forgot to put name of the class as a parameter?</xsl:message>
		</xsl:if>
		<xsl:if test="not(/d:design/d:class[@name=$className]/@deviceLogicTypeName)">
				<xsl:message terminate="yes">Class '<xsl:value-of select="$className"/>' hasnt got device logic</xsl:message>
		</xsl:if>
		/* This is device header stub */


		#ifndef __<xsl:value-of select="fnc:Base_DClassName($className)" />__H__
		#define __<xsl:value-of select="fnc:Base_DClassName($className)" />__H__

		#include &lt;vector&gt;
		#include &lt;boost/thread/mutex.hpp&gt;

		#include &lt;statuscode.h&gt;
		#include &lt;uadatetime.h&gt;
		#include &lt;session.h&gt;
		
		
		#include &lt;DRoot.h&gt;
		#include &lt;Configuration.hxx&gt;

		#include &lt;<xsl:value-of select="fnc:Base_DClassName($className)" />.h&gt;

		/* forward decl for AddressSpace */
		namespace AddressSpace { class
		<xsl:value-of select="fnc:ASClassName($className)" />
		; }
		/* forward decl for Parent */
		<xsl:if test="fnc:getCountParentClassesAndRoot(/,$className)=1">
		#include &lt;<xsl:value-of select="fnc:getParentDeviceClass(/,$className)"/>.h&gt;
		</xsl:if>

		namespace Device
		{
		
		/* forward declarations (comes from design.class.hasObjects) */
		<xsl:for-each select="/d:design/d:class[@name=$className]/d:hasobjects">
		class <xsl:value-of select="fnc:DClassName(@class)"/>;
		</xsl:for-each>

		<xsl:for-each select="/d:design/d:class[@name=$className]">
			<xsl:call-template name="deviceHeader" />
		</xsl:for-each>


		}

		#endif // include guard
	</xsl:template>






</xsl:transform>
