
/* This is device header stub */


#ifndef __DAgent__H__
#define __DAgent__H__

#include <vector>
#include <list>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DAgent.h>


namespace Device
{

// fwd
class DAgentInternal;


class
    DAgent
    : public Base_DAgent
{

public:
    /* sample constructor */
    explicit DAgent ( const Configuration::Agent & config

                      , DRoot * parent

                    ) ;
    /* sample dtr */
    ~DAgent ();




    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DAgent( const DAgent & ) = delete;
    DAgent& operator=(const DAgent &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void openSession ();
    DAgentInternal * getOpaque () { return m_internal.get(); }
    void addMonitoredItem (DDataItem *d) { m_monitoredItems.push_back(d); }
    void refreshOneItem ();

private:

    std::unique_ptr<DAgentInternal> m_internal;
    std::string m_address;
    std::string m_community;
    //! Items in the list will be periodically polled (=monitored)
    std::list<DDataItem*> m_monitoredItems;
    //! Keeps last polled item.
    decltype(m_monitoredItems.begin()) m_nextItem;

};





}

#endif // include guard
