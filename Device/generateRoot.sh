source ../Design/functions.sh

OUTPUT=include/DRoot.h
transformDesign designToRootHeader.xslt $OUTPUT 0 1
if [ $? -ne 0 ]; then 
	exit 1 
fi

OUTPUT=src/DRoot.cpp
transformDesign designToRootBody.xslt $OUTPUT 0 1
if [ $? -ne 0 ]; then 
	exit 1 
fi
