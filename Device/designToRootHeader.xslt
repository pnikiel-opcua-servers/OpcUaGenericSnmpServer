<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Generic OPC UA Server Framework

(C) CERN 2014-2015

Authors:
Piotr Nikiel <piotr@nikiel.info>

-->


<xsl:transform version="2.0"
	xmlns:xml="http://www.w3.org/XML/1998/namespace" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:d="http://www.example.org/Design" xmlns:fnc="http://MyFunctions"
	xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:include href="../Design/CommonFunctions.xslt" />
	<xsl:output method="text"></xsl:output>


	<xsl:template match="/">
	#ifndef DROOT_H_
	#define DROOT_H_
	
	#include &lt;uabase.h&gt; /* only for UA_DISABLE_COPY */

	#include &lt;vector&gt;
	namespace Device
	{
	
	<xsl:for-each select="/d:design/d:root/d:hasobjects">
	<xsl:variable name="className"><xsl:value-of select="@class"/></xsl:variable>
	<xsl:if test="/d:design/d:class[@name=$className]/@deviceLogicTypeName">
	class <xsl:value-of select="fnc:DClassName(@class)"/>;
	</xsl:if>
	</xsl:for-each>
	
	class DRoot
	{
	public:
		/* yes, its a singleton */
		static DRoot * getInstance ();
		
		UA_DISABLE_COPY(DRoot);
		/* Todo try C++11 disable copy */
		DRoot ();
		~DRoot ();
		
		/* To gracefully quit */
		void unlinkAllChildren () const;
		
		/* For constructing the tree of devices and for browsing children. */
		<xsl:for-each select="/d:design/d:root/d:hasobjects">
		<xsl:variable name="className"><xsl:value-of select="@class"/></xsl:variable>
		<xsl:if test="/d:design/d:class[@name=$className]/@deviceLogicTypeName">
		void add (<xsl:value-of select="fnc:DClassName(@class)"/> *d) { m_<xsl:value-of select="fnc:DClassName(@class)"/>s.push_back (d); }
		const std::vector&lt;<xsl:value-of select="fnc:DClassName(@class)"/> * &gt; &amp; <xsl:value-of select="lower-case(@class)"/>s () const { return m_<xsl:value-of select="fnc:DClassName(@class)"/>s; }
		</xsl:if> 
		</xsl:for-each>
		
		
		/* find methods for children */
		<xsl:for-each select="/d:design/d:root/d:hasobjects">
		<xsl:variable name="class"><xsl:value-of select="@class"/></xsl:variable>
		<xsl:for-each select="/d:design/d:class[@name=$class]/d:cachevariable[@isKey='true']">
		
		<xsl:value-of select="fnc:DClassName($class)"/> * get<xsl:value-of select="$class"/>By<xsl:value-of select="fnc:capFirst(@name)"/> (
		<xsl:choose>
		<xsl:when test="@dataType='UaString'">std::string </xsl:when>
		<xsl:otherwise><xsl:value-of select="@dataType"/></xsl:otherwise>
		</xsl:choose>
		 key) const;
		
		</xsl:for-each>
		</xsl:for-each>
		
		/* Here you can put your custom code, e.g. for browsing objects which are children of root elements */
		
	private:
		static DRoot *m_instance; /* it's a singleton class */
		
		/* Pointers to children */
		<xsl:for-each select="/d:design/d:root/d:hasobjects">
		<xsl:variable name="className"><xsl:value-of select="@class"/></xsl:variable>
		<xsl:if test="/d:design/d:class[@name=$className]/@deviceLogicTypeName">
		std::vector&lt;<xsl:value-of select="fnc:DClassName(@class)"/> *&gt; m_<xsl:value-of select="fnc:DClassName(@class)"/>s;
		</xsl:if>
		</xsl:for-each>
			
	};

	}
	#endif // include guard
	</xsl:template>






</xsl:transform>
