/*
 * ASBoc.cc
 *
 *  Created on: Jun 12, 2014
 *      Author: pnikiel
 */

#include <stdlib.h>
#include <stdexcept>
#include <iostream>
#include <uanodeid.h>

#include "ASUtils.h"

using namespace std;

void abort_with_message(const char* file, int line, const char* message)
{
	std::cout << "ABORT WITH MESSAGE:" << std::endl;
	std::cout << message << std::endl;
	std::cout << "At " << file << ":" << line << std::endl;
	abort ();
}

namespace AddressSpace
{



}
