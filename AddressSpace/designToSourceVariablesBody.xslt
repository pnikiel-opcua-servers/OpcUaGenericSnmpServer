<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Generic OPC UA Server Framework 
(C) CERN 2014-2015

Created:
September 2014

Authors: 
Piotr Nikiel <piotr@nikiel.info>

-->


<xsl:transform version="2.0" 
xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:d="http://www.example.org/Design" 
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd "

>
	<xsl:include href="../Design/CommonFunctions.xslt" />
	<xsl:output method="text"/>
	<xsl:param name="xsltFileName"/>

	
		<xsl:template  name="ReadJob">
	<xsl:param name="className"/>
	<xsl:param name="variableName"/>
	
	class IoJob_<xsl:value-of select="$className"/>_READ_<xsl:value-of select="$variableName"/> : public UaThreadPoolJob
	{
		public:
		IoJob_<xsl:value-of select="$className"/>_READ_<xsl:value-of select="$variableName"/> (
			IOManagerCallback *callback,
			OpcUa_UInt32 hTransaction,
			OpcUa_UInt32 callbackHandle,
			const UaNode* parentObjectNode
		):
			m_callback(callback),
			m_hTransaction(hTransaction),
			m_callbackHandle (callbackHandle),
			m_parentObjectNode (parentObjectNode)
		{}
		virtual void execute ()
		{
			cout &lt;&lt; "IOJOB execute!" &lt;&lt; endl;
			cout &lt;&lt; "hTransaction:"&lt;&lt;m_hTransaction&lt;&lt;" cbkhandle "&lt;&lt; m_callbackHandle&lt;&lt;endl;
			UaStatus s;
			<xsl:value-of select="@dataType"/> value;
			UaDateTime sourceTime;
			// Obtain Device Logic object
			const <xsl:value-of select="fnc:ASClassName($className)"/> *addressSpaceObject;
			addressSpaceObject = dynamic_cast&lt;const <xsl:value-of select="fnc:ASClassName($className)"/>* &gt; ( m_parentObjectNode );
			if (addressSpaceObject == m_parentObjectNode)
			{ /* OK. Proper cast. */
				Device::<xsl:value-of select="fnc:DClassName($className)"/> *device = addressSpaceObject->getDeviceLink();
				if (device != 0)
				{
					/* OK - Device Logic linked. */
					<xsl:choose >
					<xsl:when test="@addressSpaceReadUseMutex='of_this_operation'">
					device->lockVariableRead_<xsl:value-of select="@name"/> ();
					</xsl:when>
					<xsl:when test="@addressSpaceReadUseMutex='of_this_variable'">
					device->lockVariable_<xsl:value-of select="@name"/> ();
					</xsl:when>
					<xsl:when test="@addressSpaceReadUseMutex='of_containing_object'">
					device->lock();
					</xsl:when>
					<xsl:when test="@addressSpaceReadUseMutex='of_parent_of_containing_object'">
					device->getParent()->lock();
					</xsl:when>
					</xsl:choose>
					try
					{
						s = device->read<xsl:value-of select="fnc:capFirst($variableName)"/> (value, sourceTime );
					}
					catch (...)
					{
						/* TODO -- how to signalize error from here */
						std::cout &lt;&lt; "An exception was thrown from read<xsl:value-of select="fnc:capFirst($variableName)"/>" &lt;&lt; std::endl;
						s = OpcUa_BadInternalError;
					}
					<xsl:choose >
					<xsl:when test="@addressSpaceReadUseMutex='of_this_operation'">
					device->unlockVariableRead_<xsl:value-of select="@name"/> ();
					</xsl:when>
					<xsl:when test="@addressSpaceReadUseMutex='of_this_variable'">
					device->unlockVariable_<xsl:value-of select="@name"/> ();
					</xsl:when>
					<xsl:when test="@addressSpaceReadUseMutex='of_containing_object'">
					device->unlock();
					</xsl:when>
					<xsl:when test="@addressSpaceReadUseMutex='of_parent_of_containing_object'">
					device->getParent()->unlock();
					</xsl:when>
					</xsl:choose>
				}
				else
					s = OpcUa_BadInternalError;
			}
			else
				s = OpcUa_BadInternalError;
			UaDataValue result (UaVariant(value), s.statusCode(), sourceTime, UaDateTime::now());
			// get appropriate object
			s = m_callback->finishRead (
				m_hTransaction,
				m_callbackHandle,
				result
			);
			cout &lt;&lt; "After finishRead status:" &lt;&lt; s.toString().toUtf8() &lt;&lt; endl;
			
		}
		private:
			IOManagerCallback *m_callback;
			OpcUa_UInt32 m_hTransaction;
			OpcUa_UInt32 m_callbackHandle;
			const UaNode* m_parentObjectNode;
	};
	
	</xsl:template>

	<xsl:template  name="WriteJob">
	<xsl:param name="className"/>
	
	class IoJob_<xsl:value-of select="$className"/>_WRITE_<xsl:value-of select="@name"/> : public UaThreadPoolJob
	{
		public:
		IoJob_<xsl:value-of select="$className"/>_WRITE_<xsl:value-of select="@name"/> (
			IOManagerCallback *callback,
			OpcUa_UInt32 hTransaction,
			OpcUa_UInt32 callbackHandle,
			const UaNode* parentObjectNode,
			OpcUa_WriteValue* writeValue
		):
			m_callback(callback),
			m_hTransaction(hTransaction),
			m_callbackHandle (callbackHandle),
			m_parentObjectNode (parentObjectNode),
			m_variant (writeValue->Value.Value)
	
		{

		}
		virtual void execute ()
		{
			cout &lt;&lt; "IOJOB execute!" &lt;&lt; endl;
			cout &lt;&lt; "hTransaction:"&lt;&lt;m_hTransaction&lt;&lt;" cbkhandle "&lt;&lt; m_callbackHandle&lt;&lt;endl;
			UaStatus s;
			
			<xsl:value-of select="@dataType"/> value;
			<xsl:choose>
			<xsl:when test="@dataType='UaVariant'">
			value = m_variant;
			if (true)
			{
			</xsl:when>
			<xsl:otherwise>
			if (m_variant.type() == <xsl:value-of select="fnc:dataTypeToBuiltinType(@dataType)"/>)
			{
				m_variant.<xsl:value-of select="fnc:dataTypeToVariantConverter(@dataType)"/> ( value );
			</xsl:otherwise>
			</xsl:choose>

				// Obtain Device Logic object
				const <xsl:value-of select="fnc:ASClassName($className)"/> *addressSpaceObject;
				addressSpaceObject = dynamic_cast&lt;const <xsl:value-of select="fnc:ASClassName($className)"/>* &gt; ( m_parentObjectNode );
				if (addressSpaceObject == m_parentObjectNode)
				{ /* OK. Proper cast. */
					Device::<xsl:value-of select="fnc:DClassName($className)"/> *device = addressSpaceObject->getDeviceLink();
					if (device != 0)
					{
					<xsl:choose >
					<xsl:when test="@addressSpaceWriteUseMutex='of_this_operation'">
					device->lockVariableWrite_<xsl:value-of select="@name"/> ();
					</xsl:when>
					<xsl:when test="@addressSpaceWriteUseMutex='of_this_variable'">
					device->lockVariable_<xsl:value-of select="@name"/> ();
					</xsl:when>
					<xsl:when test="@addressSpaceWriteUseMutex='of_containing_object'">
					device->lock();
					</xsl:when>
					<xsl:when test="@addressSpaceWriteUseMutex='of_parent_of_containing_object'">
					device->getParent()->lock();
					</xsl:when>
					</xsl:choose>
						/* OK - Device Logic linked. */
						try
						{
							s = device->write<xsl:value-of select="fnc:capFirst(@name)"/> (value );
						}
						catch (...)
						{
							/* TODO -- how to signalize error from here */
							std::cout &lt;&lt; "An exception was thrown from read<xsl:value-of select="fnc:capFirst(@name)"/>" &lt;&lt; std::endl;
							s = OpcUa_BadInternalError;
						}
					<xsl:choose >
					<xsl:when test="@addressSpaceWriteUseMutex='of_this_operation'">
					device->unlockVariableWrite_<xsl:value-of select="@name"/> ();
					</xsl:when>
					<xsl:when test="@addressSpaceWriteUseMutex='of_this_variable'">
					device->unlockVariable_<xsl:value-of select="@name"/> ();
					</xsl:when>
					<xsl:when test="@addressSpaceWriteUseMutex='of_containing_object'">
					device->unlock();
					</xsl:when>
					<xsl:when test="@addressSpaceWriteUseMutex='of_parent_of_containing_object'">
					device->getParent()->unlock();
					</xsl:when>
					</xsl:choose>
						
					}
					else
						s = OpcUa_BadInternalError;
				}
				else
					s = OpcUa_BadInternalError;
			}
			else
			    s = OpcUa_BadDataEncodingInvalid;
			UaDataValue result (UaVariant(value), s.statusCode(), UaDateTime::now(), UaDateTime::now());
			// get appropriate object
			s = m_callback->finishWrite (
				m_hTransaction,
				m_callbackHandle,
				s
			);
			cout &lt;&lt; "After finishRead status:" &lt;&lt; s.toString().toUtf8() &lt;&lt; endl;
			
		}
		private:
			IOManagerCallback *m_callback;
			OpcUa_UInt32 m_hTransaction;
			OpcUa_UInt32 m_callbackHandle;
			const UaNode* m_parentObjectNode;
			OpcUa_WriteValue* m_writeValue;
			UaVariant m_variant;
	};
	
	</xsl:template>

	<xsl:template match="/">	

	<xsl:value-of select="fnc:header($xsltFileName)"/>

	#include &lt;iomanager.h&gt;
	#include &lt;SourceVariables.h&gt;
	#include &lt;iostream&gt;
	
	<xsl:for-each select="/d:design/d:class">
	<xsl:if test="count(d:sourcevariable)>0">
	#include &lt;<xsl:value-of select="fnc:ASClassName(@name)"/>.h&gt;
	<xsl:if test="@deviceLogicTypeName">
	#include &lt;<xsl:value-of select="fnc:DClassName(@name)"/>.h&gt;
	</xsl:if>
	</xsl:if>
	</xsl:for-each>
	
	using namespace std;

	namespace AddressSpace
	{

	static UaThreadPool *sourceVariableThreads;
	void SourceVariables_initSourceVariablesThreadPool ()
	{
		sourceVariableThreads = new UaThreadPool (0,10);
	}


		<xsl:for-each select="/d:design/d:class">
		<xsl:variable name="className"><xsl:value-of select="@name"/></xsl:variable>
		<xsl:for-each select="d:sourcevariable">
			<xsl:if test="@addressSpaceRead='asynchronous' or @addressSpaceRead='synchronous' ">
			<xsl:call-template name="ReadJob">
			<xsl:with-param name="className"><xsl:value-of select="$className"/></xsl:with-param>
			<xsl:with-param name="variableName"><xsl:value-of select="@name"/></xsl:with-param>
			</xsl:call-template>
			</xsl:if>
			
			<xsl:if test="@addressSpaceWrite='asynchronous' or @addressSpaceWrite='synchronous'">
			<xsl:call-template name="WriteJob">
			<xsl:with-param name="className"><xsl:value-of select="$className"/></xsl:with-param>

			</xsl:call-template>

			</xsl:if>
		</xsl:for-each>
		</xsl:for-each>

UaStatus SourceVariables_spawnIoJobRead (		
		ASSourceVariableJobId jobId,
		IOManagerCallback *callback,
		OpcUa_UInt32 hTransaction,
		OpcUa_UInt32        callbackHandle,
		const UaNode *parentNode
	)
	{
		switch (jobId)
		{
		<xsl:for-each select="/d:design/d:class">
		<xsl:variable name="className"><xsl:value-of select="@name"/></xsl:variable>
		<xsl:for-each select="d:sourcevariable">
			<xsl:if test="@addressSpaceRead='asynchronous' or @addressSpaceRead='synchronous'">
			
			case ASSOURCEVARIABLE_<xsl:value-of select="$className"/>_READ_<xsl:value-of select="@name"/>:
			{
				<xsl:choose>
				<xsl:when test="@addressSpaceRead='asynchronous'">
				IoJob_<xsl:value-of select="$className"/>_READ_<xsl:value-of select="@name"/> *job =
				new IoJob_<xsl:value-of select="$className"/>_READ_<xsl:value-of select="@name"/> (
				callback,
				hTransaction,
				callbackHandle,
				parentNode
				); 
				sourceVariableThreads-&gt;addJob (job);
				</xsl:when>
				<xsl:when test="@addressSpaceRead='synchronous'">
				IoJob_<xsl:value-of select="$className"/>_READ_<xsl:value-of select="@name"/> job (
				callback,
				hTransaction,
				callbackHandle,
				parentNode
				); 
				job.execute();
				</xsl:when>
				<xsl:otherwise>
				<xsl:message terminate="yes">Something got wrong.</xsl:message>
				</xsl:otherwise>
				</xsl:choose>
			}
				return OpcUa_Good;

			</xsl:if>
		</xsl:for-each>
		</xsl:for-each>
					default:
				return OpcUa_Bad;
		}
	}
	
	UaStatus SourceVariables_spawnIoJobWrite (		
		ASSourceVariableJobId jobId,
		IOManagerCallback *callback,
		OpcUa_UInt32 hTransaction,
		OpcUa_UInt32        callbackHandle,
		const UaNode *parentNode,
		OpcUa_WriteValue*   pWriteValue
	)
	{
		switch (jobId)
		{
		<xsl:for-each select="/d:design/d:class">
		<xsl:variable name="className"><xsl:value-of select="@name"/></xsl:variable>
		<xsl:for-each select="d:sourcevariable">
			<xsl:if test="@addressSpaceWrite='asynchronous' or @addressSpaceWrite='synchronous'">
			
			case <xsl:value-of select="fnc:sourceVariableWriteJobId($className,@name)"/>:
			{
				<xsl:choose>
				<xsl:when test="@addressSpaceWrite='asynchronous'">
			
				IoJob_<xsl:value-of select="$className"/>_WRITE_<xsl:value-of select="@name"/> *job =
				new IoJob_<xsl:value-of select="$className"/>_WRITE_<xsl:value-of select="@name"/> (
				callback,
				hTransaction,
				callbackHandle,
				parentNode,
				pWriteValue
				); 
				sourceVariableThreads-&gt;addJob (job);
				</xsl:when>
				<xsl:when test="@addressSpaceWrite='synchronous'">
				
				IoJob_<xsl:value-of select="$className"/>_WRITE_<xsl:value-of select="@name"/> job (
				callback,
				hTransaction,
				callbackHandle,
				parentNode,
				pWriteValue
				); 
				job.execute();
				
				</xsl:when>
				<xsl:otherwise>
				<xsl:message terminate="yes">Something got wrong.</xsl:message>
				</xsl:otherwise>
				</xsl:choose>
				
				
			}
				return OpcUa_Good;

			</xsl:if>
		</xsl:for-each>
		</xsl:for-each>
					default:
				return OpcUa_Bad;
		}
	}

	<!-- Iterate for all sourcevariables -->
	<xsl:for-each select="/d:design/d:class">
	<xsl:variable name="className"><xsl:value-of select="@name"/></xsl:variable>
	<xsl:for-each select="d:sourcevariable">
		<xsl:if test="@addressSpaceRead='asynchronous'">

		</xsl:if>
	</xsl:for-each>
	</xsl:for-each>
	
	}

	</xsl:template>
	


</xsl:transform>
