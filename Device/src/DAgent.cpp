
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DAgent.h>
#include <ASAgent.h>


#include <DDataItem.h>

#include <DAgentInternal.h>

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DAgent::DAgent (const Configuration::Agent & config

                , DRoot * parent

               ):
    Base_DAgent( config

                 , parent

               ),
	m_internal (new DAgentInternal ()),
	m_address (config.address()),
	m_community (config.community()),
	m_monitoredItems (),
	m_nextItem (m_monitoredItems.end())

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
    /*
     * Initialize a "session" that defines who we're going to talk to
     */
	snmp_session session;
    snmp_sess_init( &session );                   /* set up defaults */
    session.peername = strdup(m_address.c_str());

    /* set up the authentication parameters for talking to the server */

    /* set the SNMP version number */
    session.version = SNMP_VERSION_1;

    /* set the SNMPv1 community name used for authentication */
    session.community = (u_char*)(m_community.c_str());
    session.community_len = m_community.length();


    /*
     * Open the session
     */
    //SOCK_STARTUP;
    m_internal->session = snmp_open(&session);                     /* establish the session */
    free (session.peername);

    if (!m_internal->session)
    {
      snmp_sess_perror("ack", &session);
      //SOCK_CLEANUP;
      exit(1);
    }
}

/* sample dtr */
DAgent::~DAgent ()
{
	// TODO : session.peername was strduped, needs freeing
	snmp_close( m_internal->session );

}

/* delegators for cachevariables and externalvariables */



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333


void DAgent::openSession ()
{



}

void DAgent::refreshOneItem ()
{
	if (m_nextItem == m_monitoredItems.end())
		m_nextItem = m_monitoredItems.begin();
	if (m_nextItem != m_monitoredItems.end())
	{
		(*m_nextItem)->refresh();
		m_nextItem++;
	}
}

}


