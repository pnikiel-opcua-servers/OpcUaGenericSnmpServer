<?xml version="1.0" encoding="UTF-8"?>
<!-- Authors:      Piotr Nikiel <piotr@nikiel.info> -->
<!-- Created on:   28 Jan 2015                      -->
<!-- (C) CERN 2014-2015                                  -->
<xsl:transform version="2.0" xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:d="http://www.example.org/Design"
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">

<xsl:param name="remove_makeSetGet" required="no"/>
<xsl:param name="add_nullPolicy" required="no"/>



	<xsl:template match="d:cachevariable">
	<xsl:copy>
		<xsl:for-each select="@*">
			<xsl:if test="$remove_makeSetGet='' or ($remove_makeSetGet='yes' and name()!='makeSetGet')">
				<xsl:copy/>
			</xsl:if>
		</xsl:for-each>
		
		<xsl:if test="$add_nullPolicy!='' and not(@nullPolicy)">
			<xsl:attribute name="nullPolicy"><xsl:value-of select="$add_nullPolicy"/></xsl:attribute>
		</xsl:if>
	<!--  <xsl:apply-templates select="@* | node()"/> -->
	</xsl:copy>

	</xsl:template>
	
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

	<xsl:template match="/">
	<!-- do some checks on input params -->
	<xsl:if test="$remove_makeSetGet!='' and $remove_makeSetGet!='yes'">
		<xsl:message terminate="yes">Param 'remove_makeSetGet' can be either empty or 'yes'; no other values allowed. [given value is <xsl:value-of select="$remove_makeSetGet"/>]</xsl:message>
	</xsl:if>
	
	<xsl:if test="$add_nullPolicy!='' and $add_nullPolicy!='nullAllowed' and $add_nullPolicy!='nullForbidden'">
		<xsl:message terminate="yes">Param 'add_nullPolicy' can be either empty or 'nullAllowed' or 'nullForbidden'; no other values allowed. [given value is <xsl:value-of select="$add_nullPolicy"/>]</xsl:message>
	</xsl:if>
	
	<xsl:apply-templates/>
	</xsl:template>

</xsl:transform>
