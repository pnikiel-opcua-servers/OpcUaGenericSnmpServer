<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Generic OPC UA Server Framework 
(C) CERN 2014-2015

Created:
September 2014

Authors: 
Piotr Nikiel <piotr@nikiel.info>

-->

<xsl:transform version="2.0" 
xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:d="http://www.example.org/Design" 
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd "

>
	<xsl:include href="../Design/CommonFunctions.xslt" />
	<xsl:output method="text"/>
	<xsl:param name="xsltFileName"/>

	<xsl:template  name="IOJobRead">
	<xsl:param name="className"/>
	<xsl:param name="variableName"/>
	
	class IoJob_<xsl:value-of select="$className"/>_Read_<xsl:value-of select="$variableName"/> : public UaThreadPoolJob
	{
		
	};
	
	</xsl:template>


	<xsl:template match="/">	
	#ifndef __IOMANAGERS_H__
	#define __IOMANAGERS_H__
	<xsl:value-of select="fnc:header($xsltFileName)"/>

	#include &lt;iomanager.h&gt;
	#include &lt;uathreadpool.h&gt;
	#include &lt;uabasenodes.h&gt;

	namespace AddressSpace
	{
	
	<!-- Generate IO JOB directory -->
	enum ASSourceVariableJobId
	{
		ASSOURCEVARIABLE_NOTHING
	<!-- Iterate for all sourcevariables -->
	<xsl:for-each select="/d:design/d:class">
	<xsl:variable name="className"><xsl:value-of select="@name"/></xsl:variable>
	<xsl:for-each select="d:sourcevariable">
		<xsl:if test="@addressSpaceRead='asynchronous' or @addressSpaceRead='synchronous'">
			,<xsl:value-of select="fnc:sourceVariableReadJobId($className,@name)"/>
		</xsl:if>
		<xsl:if test="@addressSpaceWrite='asynchronous' or @addressSpaceWrite='synchronous'">
			,<xsl:value-of select="fnc:sourceVariableWriteJobId($className,@name)"/>
		</xsl:if>
	</xsl:for-each>
	</xsl:for-each>
	};
	
	void SourceVariables_initSourceVariablesThreadPool ();
	
	UaStatus SourceVariables_spawnIoJobRead (
		ASSourceVariableJobId jobId,
		IOManagerCallback *callback,
		OpcUa_UInt32 hTransaction,
		OpcUa_UInt32        callbackHandle,
		const UaNode *parentNode
		
		);
		
	UaStatus SourceVariables_spawnIoJobWrite (
		ASSourceVariableJobId jobId,
		IOManagerCallback *callback,
		OpcUa_UInt32 hTransaction,
		OpcUa_UInt32        callbackHandle,
		const UaNode *parentNode,
		OpcUa_WriteValue*   pWriteValue
		
		
		);
		

	}

#endif // include guard

	</xsl:template>
	


</xsl:transform>
