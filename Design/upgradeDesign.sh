#!/bin/sh

source ./functions.sh

OUTPUT=Design.xml.upgraded

echo "Formatting your design file ..."
./formatDesign.sh

transformDesign designToUpgradedDesign.xslt $OUTPUT 0 0 "$1"
if [ $? -ne 0 ]; then 
	echo "Transform failed; not continuing."
	exit 1 
fi

echo "Formatting the upgraded file "
xmllint --format $OUTPUT > $OUTPUT.formatted 

if [ $? -ne 0 ]; then 
	echo "Formatting the upgraded file failed, not continuing. "
	exit 1 
fi
echo "Now running merge-tool. Please merge the upgraded changed"
kdiff3 -o Design.xml Design.xml $OUTPUT.formatted 

if [ $? -ne 0 ]; then 
	echo "Merge-tool failed; your design was not upgraded."
	exit 1 
fi

echo "Merge-tool was successful. Apparently you have upgraded your file"
