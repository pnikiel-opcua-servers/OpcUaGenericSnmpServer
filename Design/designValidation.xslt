<?xml version="1.0" encoding="UTF-8"?>
<!-- Authors:      Piotr Nikiel <piotr@nikiel.info> -->
<!-- Created on:   28 Jan 2015                      -->
<!-- (C) CERN 2014-2015                                  -->

<!--
This XSLT is not used to generate any useful content.
Its sole purpose is to validate the design file as a 2nd line of defence, that is, to find
errors which weren't possible to be found using schema-conformance check (or were very difficult to be done thT way).

The validation is successful when the XSLT processor finishes without error.
The output is not relevant.

If the processor finishes with error (most likely due to a message with terminate="yes") then the validation wasn't successful.
 
 -->
<xsl:transform version="2.0" xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:d="http://www.example.org/Design"
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">



<xsl:template name="cachevariable">
<xsl:param name="className"/>
	<xsl:if test="@makeSetGet">
		<!-- task: OPCUA-120 setters/getters should be always generated, this option should go deprecated -->
		<xsl:message>WARNING (at class='<xsl:value-of select="$className"/>' variable='<xsl:value-of select="@name"/>'): makeSetGet attribute of d:cachevariable is deprecated since GenericOpcUaServer-0.93 and considered always true. Please suppress this attribute from your design file. (Hint: you can use upgradeDesign.sh tool for that)</xsl:message>
	</xsl:if>
	<xsl:if test="@nullPolicy!='nullAllowed' and @initializeWith!='configuration' and not(@initialValue)">
		<xsl:message terminate="yes">ERROR (at class='<xsl:value-of select="$className"/>' variable='<xsl:value-of select="@name"/>'): these settings are invalid. When nullPolicy doesn't allow NULL, for valueAndStatus initializer you have to have initialValue attribute. </xsl:message>
	</xsl:if>
</xsl:template>

<xsl:template match="d:class">
	<xsl:variable name="className">
		<xsl:value-of select="@name" />
	</xsl:variable>
	<xsl:for-each select="d:cachevariable">
		<xsl:call-template name="cachevariable">
		<xsl:with-param name="className"><xsl:value-of select="$className"/></xsl:with-param>
		</xsl:call-template>
	</xsl:for-each>
</xsl:template>


<xsl:template match="/">
<xsl:apply-templates/>
</xsl:template>


</xsl:transform>
