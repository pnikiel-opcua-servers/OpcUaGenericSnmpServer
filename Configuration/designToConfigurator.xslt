<?xml version="1.0" encoding="UTF-8"?>
<!-- Authors:      Piotr Nikiel <piotr@nikiel.info> -->
<!-- Created on:   14 Jul 2014                      -->
<!-- (C) CERN 2014-2015                             -->
<xsl:transform version="2.0" xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:d="http://www.example.org/Design"
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:output method="text"></xsl:output>
	<xsl:include href="../Design/CommonFunctions.xslt" />
	<xsl:param name="xsltFileName"/>

	<xsl:template name="createDeviceLogic">
	<xsl:param name="configItem"/>
	<xsl:param name="parentDevice"/>
	<xsl:if test="@deviceLogicTypeName">
	
	Device::<xsl:value-of select="fnc:DClassName(@name)"/> *d<xsl:value-of select="fnc:DClassName(@name)"/> = new Device::<xsl:value-of select="fnc:DClassName(@name)"/> (<xsl:value-of select="$configItem"/>
	<xsl:if test="fnc:getCountParentClassesAndRoot(/,@name)=1">
	, <xsl:value-of select="$parentDevice"/>
	</xsl:if>
	);
	a<xsl:value-of select="fnc:ASClassName(@name)"/>-&gt;linkDevice( d<xsl:value-of select="fnc:DClassName(@name)"/> );
	d<xsl:value-of select="fnc:DClassName(@name)"/>-&gt;linkAddressSpace( a<xsl:value-of select="fnc:ASClassName(@name)"/> );
	<xsl:value-of select="$parentDevice"/>-&gt;add (  d<xsl:value-of select="fnc:DClassName(@name)"/> );
	
	</xsl:if>
	</xsl:template>

	
	
	<xsl:template name="hasObjects">
	<xsl:param name="parentNodeId"/>
	<xsl:param name="parentDevice"/>
	<xsl:param name="configuration" />
	<xsl:param name="containingClass" />
	<xsl:choose>
	<xsl:when test="@instantiateUsing='configuration'">
	BOOST_FOREACH(const Configuration::<xsl:value-of select="@class"/> &amp; <xsl:value-of select="@class"/>config, config.<xsl:value-of select="@class"/><xsl:if test="$containingClass=@class">1</xsl:if>())
	<!-- this funny thing with adding 1 above is when a class has hasObjects towards itself- xsdcxx then renames access method with 1 at the end in order not to confuse it with the constructor -->
		{
			<xsl:variable name="containedClass"><xsl:value-of select="@class"/></xsl:variable>
			<xsl:if test="/d:design/d:class[@name=$containedClass]/@deviceLogicTypeName">
			dItem-&gt;add(
			</xsl:if>
			configure<xsl:value-of select="@class"/> (
				<xsl:value-of select="@class"/>config,
				nm,
				asItem->nodeId()
				<xsl:if test="fnc:getCountParentClassesAndRoot(/,@class)=1">
				, dItem
				</xsl:if> 
				)
			<xsl:if test="/d:design/d:class[@name=$containedClass]/@deviceLogicTypeName">
			)
			</xsl:if>;
				

		}
	</xsl:when>
	<xsl:when test="@instantiateUsing='design'">
	/* Experimental code: instantiating from design. */
	<xsl:variable name="className"><xsl:value-of select="@class"/></xsl:variable>
	<xsl:for-each select="d:object">
	{
				Configuration::<xsl:value-of select="$className"/> c ("<xsl:value-of select="@name"/>");
				AddressSpace::<xsl:value-of select="fnc:ASClassName($className)"/> *a<xsl:value-of select="fnc:ASClassName($className)"/> = 
				new AddressSpace::AS<xsl:value-of select="$className"/>(
				<xsl:value-of select="$parentNodeId"/>, 
				nm->getTypeNodeId(AddressSpace::ASInformationModel::<xsl:value-of select="fnc:typeNumericId($className)"/>), 
				nm,
				c);
				UaStatus s = nm->addNodeAndReference(<xsl:value-of select="$parentNodeId"/>, a<xsl:value-of select="fnc:ASClassName($className)"/>, OpcUaId_HasComponent);
				if (!s.isGood())
				{
					std::cout &lt;&lt; "While addNodeAndReference from " &lt;&lt; <xsl:value-of select="$parentNodeId"/>.toString().toUtf8() &lt;&lt; " to " &lt;&lt; a<xsl:value-of select="fnc:ASClassName($className)"/>-&gt;nodeId().toString().toUtf8() &lt;&lt; " : " &lt;&lt; endl;
					ASSERT_GOOD(s);
				}
				<xsl:variable name="className"><xsl:value-of select="$className"/></xsl:variable>
				<xsl:for-each select="/d:design/d:class[@name=$className]">
				<xsl:call-template name="createDeviceLogic">
				<xsl:with-param name="configItem">c</xsl:with-param>
				<xsl:with-param name="parentDevice"><xsl:value-of select="$parentDevice"/></xsl:with-param>
				</xsl:call-template>
				</xsl:for-each>
	}
	</xsl:for-each>
	
	</xsl:when>	
	</xsl:choose>
	</xsl:template>

	<xsl:template name="configureHeader">
	<xsl:param name="class"/>
	<xsl:choose>
	<xsl:when test="/d:design/d:class[@name=$class]/@deviceLogicTypeName">Device::<xsl:value-of select="fnc:DClassName($class)"/>*</xsl:when>
	<xsl:otherwise>void</xsl:otherwise> 
	</xsl:choose>
	configure<xsl:value-of select="$class"/>( const Configuration::<xsl:value-of select="$class"/> &amp; config,
					AddressSpace::ASNodeManager *nm,
					UaNodeId parentNodeId
		<xsl:if test="fnc:getCountParentClassesAndRoot(/,$class)=1">
		, Device::<xsl:value-of select="fnc:DClassName(fnc:getParentClass(/,$class))"/> * parentDevice
		</xsl:if>
		);

	</xsl:template>

	<xsl:template name="configureObject">
	<xsl:param name="class"/>
	//! Called to create every single instance of <xsl:value-of select="$class"/><xsl:text> 
	</xsl:text>
	<xsl:choose>
	<xsl:when test="/d:design/d:class[@name=$class]/@deviceLogicTypeName">Device::<xsl:value-of select="fnc:DClassName($class)"/>*</xsl:when>
	<xsl:otherwise>void</xsl:otherwise> 
	</xsl:choose>
	configure<xsl:value-of select="$class"/>( const Configuration::<xsl:value-of select="$class"/> &amp; config,
					AddressSpace::ASNodeManager *nm,
					UaNodeId parentNodeId
		<xsl:if test="fnc:getCountParentClassesAndRoot(/,$class)=1">
		, Device::<xsl:value-of select="fnc:DClassName(fnc:getParentClass(/,$class))"/> * parentDevice
		</xsl:if>
		)
	{
		
		AddressSpace::<xsl:value-of select="fnc:ASClassName($class)"/> *asItem = 
				new AddressSpace::<xsl:value-of select="fnc:ASClassName($class)"/>(
				parentNodeId, 
				nm->getTypeNodeId(AddressSpace::ASInformationModel::<xsl:value-of select="fnc:typeNumericId($class)"/>), 
				nm, 
				<xsl:value-of select="@class"/>config);
		UaStatus s = nm->addNodeAndReference( parentNodeId, asItem, OpcUaId_HasComponent);
		if (!s.isGood())
		{
			std::cout &lt;&lt; "While addNodeAndReference from " &lt;&lt; parentNodeId.toString().toUtf8() &lt;&lt; " to " &lt;&lt; asItem-&gt;nodeId().toString().toUtf8() &lt;&lt; " : " &lt;&lt; std::endl;
			ASSERT_GOOD(s);
		}
		<xsl:if test="/d:design/d:class[@name=$class]/@deviceLogicTypeName">
		Device::<xsl:value-of select="fnc:DClassName(@name)"/> *dItem = new Device::<xsl:value-of select="fnc:DClassName(@name)"/> (config
		<xsl:if test="fnc:getCountParentClassesAndRoot(/,@name)=1">
		, parentDevice
		</xsl:if>
		);
		asItem-&gt;linkDevice( dItem );
		dItem-&gt;linkAddressSpace( asItem );
		
		<!-- TODO: this is the parent who has to add items, not the child! Otherwise the problem with parents stays unsolved. -->
		<!--  <xsl:value-of select="$parentDevice"/>-&gt;add (  d<xsl:value-of select="fnc:DClassName(@name)"/> ); --> 
		
		</xsl:if>
		
		<xsl:for-each select="/d:design/d:class[@name=$class]/d:hasobjects">
		<xsl:call-template name="hasObjects">
		<xsl:with-param name="containingClass"><xsl:value-of select="$class"/></xsl:with-param>
		</xsl:call-template>
		</xsl:for-each>
		
		<xsl:if test="/d:design/d:class[@name=$class]/@deviceLogicTypeName">
		return dItem;
		</xsl:if>
	
	}
	</xsl:template>

	<xsl:template name="nestedHas">
	<xsl:param name="owner"/>
	<xsl:param name="owned"/>
	void configure<xsl:value-of select="$owner" />Has<xsl:value-of select="$owned"/> ()
	{
	  
	}
	</xsl:template>
	
	<xsl:template match="/">

	<xsl:value-of select="fnc:header($xsltFileName)"/>
	#include &lt;iostream&gt;
	#include &lt;boost/foreach.hpp&gt;
	
	#include &lt;ASUtils.h&gt;
	#include &lt;ASInformationModel.h&gt;
	
	#include &lt;DRoot.h&gt;
	
	#include &lt;Configurator.h&gt;
	#include &lt;Configuration.hxx&gt;

	
<!-- *************************************************** -->
<!-- HEADERS OF ALL DECLARED CLASSES ******************* -->
<!-- *************************************************** -->
	<xsl:for-each select="/d:design/d:class">
	#include &lt;<xsl:value-of select="fnc:ASClassName(@name)"/>.h&gt;
	<xsl:if test="@deviceLogicTypeName">
	#include &lt;<xsl:value-of select="fnc:DClassName(@name)"/>.h&gt;
	</xsl:if>
	</xsl:for-each>
	
	<xsl:for-each select="/d:design/d:class">
	<xsl:variable name="class"><xsl:value-of select="@name"/></xsl:variable>
	<xsl:call-template name="configureHeader">
	<xsl:with-param name="class"><xsl:value-of select="$class"/></xsl:with-param>
	</xsl:call-template>
	</xsl:for-each>
	
	<xsl:for-each select="/d:design/d:class">
	<xsl:variable name="class"><xsl:value-of select="@name"/></xsl:variable>
	<xsl:call-template name="configureObject">
	<xsl:with-param name="class"><xsl:value-of select="$class"/></xsl:with-param>
	</xsl:call-template>
	</xsl:for-each>
	
<!-- *************************************************** -->
<!-- CONFIGURATOR MAIN ********************************* -->
<!-- *************************************************** -->	
	using namespace std;
	void configure (std::string fileName, AddressSpace::ASNodeManager *nm)
{
	
	std::auto_ptr&lt;Configuration::Configuration&gt; theConfiguration = Configuration::configuration(fileName);
	UaNodeId rootNode = UaNodeId(OpcUaId_ObjectsFolder, 0);
	Device::DRoot *deviceRoot = Device::DRoot::getInstance();
		
	<xsl:for-each select="/d:design/d:root/d:hasobjects">
	BOOST_FOREACH(const Configuration::<xsl:value-of select="@class"/> &amp; <xsl:value-of select="@class"/>config, theConfiguration-&gt;<xsl:value-of select="@class"/>())
	{
	<xsl:variable name="containedClass"><xsl:value-of select="@class"/></xsl:variable>
	
	<xsl:if test="/d:design/d:class[@name=$containedClass]/@deviceLogicTypeName">
	deviceRoot->add(
	</xsl:if>
	configure<xsl:value-of select="@class"/> (
				<xsl:value-of select="@class"/>config,
				nm,
				rootNode
				<xsl:if test="fnc:getCountParentClassesAndRoot(/,@class)=1">
				, deviceRoot
				</xsl:if> 
				)
	
	<xsl:if test="/d:design/d:class[@name=$containedClass]/@deviceLogicTypeName">
	)
	</xsl:if>
	;


	}
	</xsl:for-each>
	
}

<!-- *************************************************** -->
<!-- DECONFIGURATOR ************************************ -->
<!-- *************************************************** -->
	void unlinkAllDevices (AddressSpace::ASNodeManager *nm)
	{
		<xsl:for-each select="/d:design/d:class[@deviceLogicTypeName]">
		{
			std::vector&lt; AddressSpace::<xsl:value-of select="fnc:ASClassName(@name)"/> * &gt; objects;
			std::string pattern (".*");
			nm->findAllByPattern&lt;AddressSpace::<xsl:value-of select="fnc:ASClassName(@name)"/>&gt; ( nm-&gt;getNode(UaNodeId(OpcUaId_ObjectsFolder, 0)), OpcUa_NodeClass_Object, pattern, objects);
			PRINT("objects.size=");
			PRINT(objects.size());
			BOOST_FOREACH(AddressSpace::<xsl:value-of select="fnc:ASClassName(@name)"/> *a, objects)
			{
				a-&gt;unlinkDevice();
			}
		}
		</xsl:for-each>
	}
	
	</xsl:template>



</xsl:transform>
