#!/bin/sh

java -jar Design/saxon9he.jar Design/Design.xml AddressSpace/designToGeneratedCmakeAddressSpace.xslt -o:AddressSpace/cmake_generated.cmake

java -jar Design/saxon9he.jar Design/Design.xml Device/designToGeneratedCmakeDevice.xslt  -o:Device/generated/cmake_header.cmake

BUILD_TYPE=Release

if [ "$1" == "Debug" ]; then
	BUILD_TYPE="Debug"
fi

cmake28 -DCMAKE_BUILD_TYPE=$BUILD_TYPE .
