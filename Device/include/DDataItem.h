
/* This is device header stub */


#ifndef __DDataItem__H__
#define __DDataItem__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>



#include <Base_DDataItem.h>



namespace Device
{

class DDataItemInternal;



class
    DDataItem
    : public Base_DDataItem
{

public:
    /* sample constructor */
    explicit DDataItem ( const Configuration::DataItem & config

                       ) ;
    /* sample dtr */
    ~DDataItem ();




    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus writeSet (
        UaVariant &value
    );

    /* ASYNCHRONOUS !! */
    UaStatus readGet (
        UaVariant &value,
        UaDateTime &sourceTime
    );


private:
    /* Delete copy constructor and assignment operator */
    DDataItem( const DDataItem & ) = delete;
    DDataItem& operator=(const DDataItem &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void setAgent( DAgent * agent ) { m_agent = agent; }
    void refresh ();

private:
    //! This is opaque type not to include net-snmp headers in this file; they aren't very c++ friendly and break some code.
    std::unique_ptr<DDataItemInternal>  m_internal;

    const UaString m_type;
    // agent which is handling me
    DAgent * m_agent;


};





}

#endif // include guard
