#!/bin/sh

source ../Design/functions.sh


OUTPUT=include/SourceVariables.h

transformDesign designToSourceVariablesHeader.xslt $OUTPUT 0 1 
if [ $? -ne 0 ]; then 
	exit 1 
fi

OUTPUT=src/SourceVariables.cpp

transformDesign designToSourceVariablesBody.xslt $OUTPUT 0 1 
if [ $? -ne 0 ]; then 
	exit 1 
fi
