<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Generic OPC UA Server Framework 
(C) CERN 2014-2015

Created:
June 2014

Authors: 
Piotr Nikiel <piotr@nikiel.info>

-->



<xsl:transform version="2.0" xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:d="http://www.example.org/Design"
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:include href="../Design/CommonFunctions.xslt" />
	<xsl:output method="text"></xsl:output>
	 <xsl:param name="className"/>
	
	<xsl:template match="/">	
	#include &lt;iostream&gt;
	#include &lt;ASUtils.h&gt;
	#include &lt;ASInformationModel.h&gt;
	

	namespace AddressSpace
	{
	
	void ASInformationModel::createNodesOfTypes (ASNodeManager *nm)
	{
		UaObjectTypeSimple * type;
		UaStatus addStatus;
		<xsl:for-each select="/d:design/d:class">
		type = new UaObjectTypeSimple ("<xsl:value-of select="@name"/>", UaNodeId(<xsl:value-of select="fnc:typeNumericId(@name)"/>, nm->getNameSpaceIndex()), /* locale id*/UaString(""), /*is abstract*/OpcUa_False);
		addStatus = nm->addNodeAndReference(OpcUaId_BaseObjectType, type, OpcUaId_HasSubtype);
		if (!addStatus.isGood())
		{
			std::cout &lt;&lt; "While adding a type definition node: ";
			ASSERT_GOOD(addStatus);
		}
		</xsl:for-each>
	}
	
	}
	
	
	</xsl:template>



</xsl:transform>
