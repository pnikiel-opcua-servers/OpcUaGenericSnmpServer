XSLT_JAR=../Design/saxon9he.jar

# args:
# $1    xslt transform filename
# $2	output filename
# $3	if 1, will prevent from overwriting output filename, running merge-tool
# $4	if 1, will run astyle on generated file
# $5	additional params to the transform
transformDesign () {
	OUTPUT_EFFECTIVE=$2
	if [ "$3" -eq "1" ]; then
		if [ -f $2 ]; then
			OUTPUT_EFFECTIVE=$2.generated
		fi
	fi
	java -jar $XSLT_JAR ../Design/Design.xml $1 -o:$OUTPUT_EFFECTIVE xsltFileName=$1 $5
	if [ $? -ne 0 ]; then
		echo "XSLT Processor Failed."
		rm $2
		exit 1
	fi
	if [ "$4" -eq "1" ]; then
		astyle $OUTPUT_EFFECTIVE
			if [ $? -ne 0 ]; then 
				exit 1 
			fi				
	fi
	if [ $3 -eq "1" ]; then
		if [ "$OUTPUT_EFFECTIVE" != "$2" ]; then
			cmp $2 $OUTPUT_EFFECTIVE
			if [ $? != 0 ]; then
				kdiff3 -o $2 $2 $OUTPUT_EFFECTIVE
				if [ $? -ne 0 ]; then
					echo "Merge tool Failed."
					exit 1
				fi
			fi
		fi
	fi
}