#!/bin/sh

source ../Design/functions.sh

OUTPUT=include/D$1.h

transformDesign designToDeviceHeader.xslt $OUTPUT 1 1 className=$1
if [ $? -ne 0 ]; then 
	exit 1 
fi
