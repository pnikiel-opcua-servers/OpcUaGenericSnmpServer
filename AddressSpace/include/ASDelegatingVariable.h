/*
 * CASDelegatingVariable.h
 *
 *  Created on: Jul 4, 2014
 *      Author: pnikiel
 */

#ifndef CASDELEGATINGVARIABLE_H_
#define CASDELEGATINGVARIABLE_H_

#include <opcua_basedatavariabletype.h>

namespace AddressSpace
{

template<typename ObjectType>
class ASDelegatingVariable: public OpcUa::BaseDataVariableType

{
public:

	ASDelegatingVariable (
			const UaNodeId&    nodeId,
	        const UaString&    name,
	        OpcUa_UInt16       browseNameNameSpaceIndex,
	        const UaVariant&   initialValue,
	        OpcUa_Byte         accessLevel,
	        NodeManagerConfig* pNodeConfig,
	        UaMutexRefCounted* pSharedMutex = NULL):
	        	OpcUa::BaseDataVariableType (nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex),
	        	m_write(0),
	        	m_object(0) {}
	virtual ~ASDelegatingVariable () {};

	UaStatus setValue(Session* pSession, const UaDataValue& dataValue, OpcUa_Boolean checkAccessLevel = OpcUa_True)
	{
		if (m_object && m_write)
		{
			// TODO Consider whether we should update cache after that or not?
			UaStatus status =  (m_object->*m_write) (pSession, dataValue, checkAccessLevel);
			if (status.isGood())
				OpcUa::BaseDataVariableType::setValue (pSession, dataValue, checkAccessLevel);
			return status;
		}
		else
		{
			//cout << "This variable is not delegated" << endl;
			return BaseDataVariableType::setValue(pSession, dataValue, checkAccessLevel);
		}
	}
	UaStatus assignHandler (ObjectType* object, UaStatus (ObjectType::*method)(Session*, const UaDataValue&, OpcUa_Boolean))
	{
		m_object = object;
		m_write = method;
		return OpcUa_Good;
	}
private:
	UaStatus (ObjectType::* m_write)(Session* pSession, const UaDataValue& dataValue, OpcUa_Boolean checkAccessLevel);
	ObjectType* m_object;
};

}
#endif /* CASDELEGATINGVARIABLE_H_ */
