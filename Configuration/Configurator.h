/*
 * Configurator.h
 *
 *  Created on: Jun 6, 2014
 *      Author: pnikiel
 */

#ifndef CONFIGURATOR_H_
#define CONFIGURATOR_H_


#include <string>

#include <ASNodeManager.h>

void configure (std::string fileName, AddressSpace::ASNodeManager *nm);
void unlinkAllDevices (AddressSpace::ASNodeManager *nm);

/* The body for that one is generated in ConfigValidator.cpp */
bool validateDeviceTree ();


#endif /* CONFIGURATOR_H_ */
