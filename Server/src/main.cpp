/*
 * main.cpp
 *
 *  Created on: Jun 6, 2014
 *      Author: pnikiel
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <boost/program_options.hpp>
#include <boost/foreach.hpp>
#include <algorithm>

#include "uaplatformlayer.h"
#include "uathread.h"
#ifdef SUPPORT_XML_CONFIG
  #include "xmldocument.h"
#endif

#include <Configuration.hxx>
#include <Configurator.h>
#include <ASNodeManager.h>
#include <DRoot.h>
#include <DDataItem.h>
#include <DAgent.h>
#include <DFolder.h>
#include <opcserver.h>
#include <shutdown.h>
#include <SourceVariables.h>
#include "version.h"

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

using namespace std;
using namespace boost::program_options;

void recurseFolder( const Device::DFolder *folder, std::list<Device::DDataItem*> & out )
{
	std::cout << "recurseFolder, it has " << folder->dataitems().size() << " items " << std::endl;
	std::copy( folder->dataitems().begin(), folder->dataitems().end(), std::back_inserter(out) );
	BOOST_FOREACH( const Device::DFolder *fChild, folder->folders() )
	{
		recurseFolder( fChild, out );
	}
}

void getAllDataItemsOnAgent (const Device::DAgent * agent, std::list<Device::DDataItem*> & out )
{
	std::cout << "this agent has " << agent->dataitems().size() << " items " << std::endl;
	// copy all root variables
	std::copy( agent->dataitems().begin(), agent->dataitems().end(),  std::back_inserter(out) );
	BOOST_FOREACH( const Device::DFolder *fChild, agent->folders() )
	{
		recurseFolder( fChild, out );
	}



}


int OpcServerMain(const char* szAppPath, const char* configFileName, bool onlyCreateCertificate)
{
    int ret = 0;

    //- Initialize the environment --------------getAllDataItemsOnAgent
#ifdef SUPPORT_XML_CONFIG
    // Initialize the XML Parser
    UaXmlDocument::initParser();
#endif
    // Initialize the UA Stack platform layer
    ret = UaPlatformLayer::init();
    //-------------------------------------------

    if ( ret == 0 )
    {
        // Create configuration file name
        UaString sConfigFileName(szAppPath);

#ifdef SUPPORT_XML_CONFIG
        sConfigFileName += "/ServerConfig.xml";
#else
        sConfigFileName += "/ServerConfig.ini";
#endif

        //- Start up OPC server ---------------------
        // This code can be integrated into a start up
        // sequence of the application where the
        // OPC server should be integrated
        //-------------------------------------------
        // Create and initialize server object
        OpcServer* pServer = new OpcServer;
        pServer->setServerConfig(sConfigFileName, szAppPath);

		if (onlyCreateCertificate)
		{
			pServer->createCertificate();
			cout << "Create certificate only" << endl;
			return 0;
		}

        // Start server object
        ret = pServer->start();
        if ( ret != 0 )
        {
            delete pServer;
            return ret;
        }
        //-------------------------------------------

        //- Add variable to address space -----------
        // Get the default node manager for server specific nodes from the SDK

        AddressSpace::SourceVariables_initSourceVariablesThreadPool ();
        AddressSpace::ASNodeManager *nm = new AddressSpace::ASNodeManager();


        pServer->addNodeManager(nm);

        try
        {
        	configure (configFileName, nm);
        } catch (xsd::cxx::tree::parsing<char> &)
        {
        	std::cout << "There was a problem with parsing your configuration file." << std::endl;
        	std::cout << "Trying to run xmllint to show exact problem with your configuration file." << std::endl;
        	system ("xmllint --noout --schema ../Configuration/Configuration.xsd config.xml");
        	return 1;
        }
        validateDeviceTree();

        /* setup agent relationship */

        BOOST_FOREACH( Device::DAgent* agent, Device::DRoot::getInstance()->agents() )
        {
        	std::list<Device::DDataItem*> items;
        	getAllDataItemsOnAgent( agent, items );
        	std::cout << "For this agent there are " << items.size() << " data items " << std::endl;
        	BOOST_FOREACH( Device::DDataItem *item, items)
        	{
        		item->setAgent( agent );
        		agent->addMonitoredItem(item);
        	}

        }


        printf("***************************************************\n");
        printf(" Press %s to shutdown server\n", SHUTDOWN_SEQUENCE);
        printf("***************************************************\n");
        // Wait for user command to terminate the server thread.

        while(ShutDownFlag() == 0)
        {

        	/* refresh one item per every agent */
        	BOOST_FOREACH(Device::DAgent *agent, Device::DRoot::getInstance()->agents())
		{
        		agent->refreshOneItem();

		}
        	UaThread::sleep (1);

        }
        printf("***************************************************\n");
        printf(" Shutting down server\n");
        printf("***************************************************\n");

        /* For all devices: disconnect their address space links */
        Device::DRoot::getInstance()->unlinkAllChildren();
        /* For all address space items: disconnect their device links */
        unlinkAllDevices(nm);

        //- Stop OPC server -------------------------
        // This code can be integrated into a shut down
        // sequence of the application where the
        // OPC server should be integrated
        //-------------------------------------------
        // Stop the server and wait three seconds if clients are connected
        // to allow them to disconnect after they received the shutdown signal
        pServer->stop(3, UaLocalizedText("", "User shut down"));
        delete pServer;
        pServer = NULL;
        //-------------------------------------------
    }

    //- Cleanup the environment --------------
    // Cleanup the UA Stack platform layer
    UaPlatformLayer::cleanup();
#ifdef SUPPORT_XML_CONFIG
    // Cleanup the XML Parser
    UaXmlDocument::cleanupParser();
#endif
    //-------------------------------------------

    return ret;
}




int main (int argc, char *argv[])
{
	/* Instantiate singleton class */
	Device::DRoot root;

    /*
     * Initialize the SNMP library
     */
    init_snmp("opcua_snmp");

    RegisterSignalHandler();

    char szAppPath[PATH_MAX];
    readlink("/proc/self/exe", szAppPath, sizeof(szAppPath));
    szAppPath[PATH_MAX-1] = 0;
    char *pszFind = strrchr(szAppPath, '/');
    if (pszFind)
    {
        *pszFind = 0; // cut off appname
    }
    /* Parse command line */
	bool createCertificateOnly = false;
	bool printVersion = false;
	string logFile;
    options_description desc ("Allowed options");

	desc.add_options()
			("config_file", "A path to the config file")
			("create_certificate", bool_switch(&createCertificateOnly), "Create new certificate and exit" )
			("help", "Print help")
			("version", bool_switch(&printVersion), "Print version and exit")
			;
	positional_options_description p;
	p.add("config_file", 1);
	variables_map vm;
	try
	{
		store(command_line_parser(argc,argv)
				.options(desc)
				.style(command_line_style::allow_long_disguise | command_line_style::unix_style)
				.positional(p)
				.run(),
				vm);
	}
	catch (boost::exception &e)
	{
		cout << "Couldn't interpret command line, please run with -help "  << endl;
		return 1;
	}
	notify(vm);
	if (vm.count("help"))
	{
		cout << desc << endl;
		return 0;
	}

	if (printVersion)
	{
		std::cout << VERSION_STR << std::endl;
		return 0;
	}
	else
	{
		const char *configFile = "config.xml";
		if (vm.count("config_file") > 0)
			configFile = vm["config_file"].as< string > ().c_str();
		//-------------------------------------------
		// Call the OPC server main method

		int ret = OpcServerMain(szAppPath, configFile, createCertificateOnly);
		cout << "OpcServerMain() exited with " << ret << endl;

		//-------------------------------------------
	}
}
