#!/bin/sh

source ../Design/functions.sh

OUTPUT=generated/Base_D$1.cpp

transformDesign designToDeviceBaseBody.xslt $OUTPUT 0 1 className=$1
if [ $? -ne 0 ]; then 
	exit 1 
fi
