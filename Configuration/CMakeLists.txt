MESSAGE( STATUS "Design file=" ${DESIGN_FILE} )

add_custom_command(OUTPUT Configuration.xsd
	COMMAND ./generateConfiguration.sh
	DEPENDS ${DESIGN_FILE} designToConfigurationXSD.xslt validateDesign
	)

add_custom_command(OUTPUT Configuration.cxx Configuration.hxx
	COMMAND xsdcxx cxx-tree Configuration.xsd
	DEPENDS Configuration.xsd
)

add_custom_target(Configuration.hxx DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/Configuration.hxx)

add_custom_command(OUTPUT Configurator.cpp
	COMMAND ./generateConfigurator.sh
	DEPENDS ${DESIGN_FILE} designToConfigurator.xslt ./generateConfigurator.sh AddressSpace Device
)

add_custom_command(OUTPUT ConfigValidator.cpp
	COMMAND ./generateConfigValidator.sh
	DEPENDS ${DESIGN_FILE} designToConfigValidator.xslt ./generateConfigValidator.sh AddressSpace Device
)

add_library (Configuration OBJECT
	Configuration.cxx
	Configuration.hxx
	Configurator.cpp
	ConfigValidator.cpp
	)