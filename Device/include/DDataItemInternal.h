/*
 * DDataItemInternal.h
 *
 *  Created on: May 10, 2015
 *      Author: pnikiel
 */

#ifndef DEVICE_INCLUDE_DDATAITEMINTERNAL_H_
#define DEVICE_INCLUDE_DDATAITEMINTERNAL_H_

#include <vector>
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/types.h>

namespace Device
{

struct DDataItemInternal
{

std::vector<oid> oids;

};

}



#endif /* DEVICE_INCLUDE_DDATAITEMINTERNAL_H_ */
