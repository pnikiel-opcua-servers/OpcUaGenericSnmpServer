/*
 * DataBinding.h
 *
 *  Created on: May 10, 2015
 *      Author: pnikiel
 */

#ifndef DEVICE_INCLUDE_DATABINDING_H_
#define DEVICE_INCLUDE_DATABINDING_H_

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <string>
#include <statuscode.h>
#include <uavariant.h>

namespace Device
{

//! This may suggest multiple variables but in fact it operates on just one
UaStatus fromSnmpToUaVariant( const netsnmp_variable_list * var, UaVariant & v, const UaString & type);


}



#endif /* DEVICE_INCLUDE_DATABINDING_H_ */
