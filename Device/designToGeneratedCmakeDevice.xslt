<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Generic OPC UA Server Framework

(C) CERN 2014-2015

Authors:
Piotr Nikiel <piotr@nikiel.info>

-->

<xsl:transform version="2.0"
xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:d="http://www.example.org/Design"
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:output method="text"></xsl:output>
	<xsl:include href="../Design/CommonFunctions.xslt" />
	<xsl:template name="commands">
	
	add_custom_command(OUTPUT ${PROJECT_SOURCE_DIR}/Device/generated/<xsl:value-of select="fnc:Base_DClassName(@name)"/>.h 
	COMMAND ${PROJECT_SOURCE_DIR}/Device/generateBaseHeader.sh <xsl:value-of select="@name"/>
	DEPENDS ${DESIGN_FILE} ${PROJECT_SOURCE_DIR}/Device/generateBaseHeader.sh ${PROJECT_SOURCE_DIR}/Device/designToDeviceBaseHeader.xslt Configuration.hxx validateDesign
	WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/Device
	)	
	
	add_custom_command(OUTPUT ${PROJECT_SOURCE_DIR}/Device/generated/<xsl:value-of select="fnc:Base_DClassName(@name)"/>.cpp 
	COMMAND ${PROJECT_SOURCE_DIR}/Device/generateBaseBody.sh <xsl:value-of select="@name"/>
	DEPENDS ${DESIGN_FILE} ${PROJECT_SOURCE_DIR}/Device/generateBaseHeader.sh ${PROJECT_SOURCE_DIR}/Device/designToDeviceBaseBody.xslt ${PROJECT_SOURCE_DIR}/Device/generated/<xsl:value-of select="fnc:Base_DClassName(@name)"/>.h Configuration.hxx validateDesign
	WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/Device
	)	
	
	
	</xsl:template>
	<xsl:template name="simpleFileList">
	${PROJECT_SOURCE_DIR}/AddressSpace/include/<xsl:value-of select="fnc:ASClassName(@name)"/>.h
	${PROJECT_SOURCE_DIR}/AddressSpace/src/<xsl:value-of select="fnc:ASClassName(@name)"/>.cpp
	</xsl:template>
	
	<xsl:template match="/">
	
	add_custom_command(OUTPUT include/DRoot.h
	COMMAND ${PROJECT_SOURCE_DIR}/Device/generateRoot.sh
	DEPENDS ${DESIGN_FILE} validateDesign designToRootHeader.xslt
	)
	
	add_custom_command(OUTPUT src/DRoot.cpp
	COMMAND ${PROJECT_SOURCE_DIR}/Device/generateRoot.sh
	DEPENDS ${DESIGN_FILE} validateDesign designToRootBody.xslt
	)
	
	<xsl:for-each select="/d:design/d:class">
	<xsl:call-template name="commands"/>
	</xsl:for-each>
	
	set(DEVICEBASE_GENERATED_FILES
        include/DRoot.h
        src/DRoot.cpp
	<xsl:for-each select="/d:design/d:class[@deviceLogicTypeName]">
	generated/<xsl:value-of select="fnc:Base_DClassName(@name)"/>.h
	generated/<xsl:value-of select="fnc:Base_DClassName(@name)"/>.cpp
	</xsl:for-each>
	)
	
	set(DEVICE_CLASSES
	<xsl:for-each select="/d:design/d:class[@deviceLogicTypeName]">
	src/<xsl:value-of select="fnc:DClassName(@name)"/>.cpp
	</xsl:for-each>
	)

	add_custom_target(DeviceBase DEPENDS ${DEVICEBASE_GENERATED_FILES} )
	
	add_custom_target(DeviceGeneratedHeaders DEPENDS include/DRoot.h ${DEVICEBASE_GENERATED_FILES})
	
	</xsl:template>
</xsl:transform>
