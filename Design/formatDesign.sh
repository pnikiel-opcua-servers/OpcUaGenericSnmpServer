#!/bin/sh
cp Design.xml Design.xml.backup
if [ "$?" != "0" ]; then
	echo "Failed to create a backup copy of your design; check directory permissions or disk space"
	exit 1
fi
xmllint --format Design.xml > Design.xml.new
if [ "$?" != "0" ]; then
	echo "xmllint exited with error, not continuing. The original design file is untouched."
	exit 1
fi
cp Design.xml.new Design.xml
echo "Finished formatting. Your original design file is stored as Design.xml.new"


