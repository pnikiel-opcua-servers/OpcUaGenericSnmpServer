#!/bin/sh
if [ -f Makefile ]; then
	make clean
fi
find . -name CMakeCache.txt -type f -exec rm -v {} \;
find . -name CMakeFiles -type d -exec rm -vR {} \;
find . -name Makefile -type f -exec rm -v {} \;
find . -name cmake_install.cmake -type f -exec rm -v {} \;
find . -name generated_files_list.cmake -type f -exec rm -v {} \;
if [ "$1" == "--orig" ]; then
	find . -name \*.orig -type f -exec rm -v {} \;
fi
