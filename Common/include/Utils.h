/*
 * Utils.h
 *
 *  Created on: Oct 22, 2014
 *      Author: pnikiel
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <sys/time.h>
#include <sstream>
#include <stdexcept>
#include <uadatetime.h>

class Utils
{
public:
template<typename T>
static std::string toString (const T t)
{
        std::ostringstream oss;
        oss << t;
        return oss.str ();
}

template<typename T>
static std::string toHexString (const T t)
{
        std::ostringstream oss;
        oss << std::hex << (unsigned long)t << std::dec;
        return oss.str ();
}

static unsigned int fromHexString (const std::string &s)
{
	unsigned int x;
	std::istringstream iss (s);
	iss >> std::hex >> x;
	if (iss.bad())
		throw std::runtime_error ("Given string '"+s+"' not convertible from hex to uint");
	return x;
}

};


#endif /* UTILS_H_ */
