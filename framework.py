#!/usr/bin/python

import os
import sys

#  TODO: get to initial directory

argc=len(sys.argv)
if argc<2:
  print "Please give command, one of: "
  print "check_consistency"
  print "setup_svn_ignore"
  sys.exit(1)
cmd=sys.argv[1]

if cmd=='check_consistency':
  os.system('cd FrameworkInternals; python manage_files.py --mode=check_consistency; cd ..')
elif cmd=='setup_svn_ignore':
  os.system('cd FrameworkInternals; python manage_files.py --mode=setup_svn_ignore; cd ..')
elif cmd=='design_vs_device':
  os.system('cd FrameworkInternals; python manage_files.py --mode=design_vs_device; cd ..')    
else:
  raise Exception ('command unknown: '+cmd)



