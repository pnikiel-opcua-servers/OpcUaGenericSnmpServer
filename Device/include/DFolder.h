
/* This is device header stub */


#ifndef __DFolder__H__
#define __DFolder__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DFolder.h>


namespace Device
{




class
    DFolder
    : public Base_DFolder
{

public:
    /* sample constructor */
    explicit DFolder ( const Configuration::Folder & config

                     ) ;
    /* sample dtr */
    ~DFolder ();




    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DFolder( const DFolder & ) = delete;
    DFolder& operator=(const DFolder &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:

private:


};





}

#endif // include guard
