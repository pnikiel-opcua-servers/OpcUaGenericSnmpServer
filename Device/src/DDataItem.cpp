
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DDataItem.h>
#include <ASDataItem.h>



#include <DFolder.h>
#include <DAgent.h>
#include <DAgentInternal.h>

#include <DDataItemInternal.h>

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <string.h>

#include <DataBinding.h>

namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DDataItem::DDataItem (const Configuration::DataItem & config

                     ):
    Base_DDataItem( config

                  ),
	m_internal( std::unique_ptr<DDataItemInternal> ( new DDataItemInternal () ) ),
	m_type (config.type().c_str()),
	m_agent(0)
/* fill up constructor initialization list here */
{

	std::cout << "ctr: converting from " << config.oid() << std::endl;
	/* parse oid */
	oid tempOid [MAX_OID_LEN];
	size_t len=MAX_OID_LEN;

	if (! snmp_parse_oid(config.oid().c_str(), tempOid, &len))
	{
		snmp_perror("");
		throw std::runtime_error("read_objid() failed");
	}
	std::cout << "The oid has len=" << len << std::endl;

	m_internal->oids.clear ();
	m_internal->oids.assign (len, 0);
	for (unsigned int i=0; i<len; i++)
		m_internal->oids[i] = tempOid[i];



}

/* sample dtr */
DDataItem::~DDataItem ()
{
}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DDataItem::writeSet (
    UaVariant &value
)
{
    return OpcUa_BadNotImplemented;
}

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DDataItem::readGet (
    UaVariant &value,
    UaDateTime &sourceTime
)
{

	    netsnmp_pdu *pdu;
	    netsnmp_pdu *response;

	    netsnmp_variable_list *vars;
	    int status;
	    int count=1;


	    /*
	     * Create the PDU for the data for our request.
	     *   1) We're going to GET the system.sysDescr.0 node.
	     */
	    pdu = snmp_pdu_create(SNMP_MSG_GET);

	    std::cout << "len=" << m_internal->oids.size() << std::endl;
	    snmp_add_null_var(pdu, &m_internal->oids[0], m_internal->oids.size());

	    /*
	     * Send the Request out.
	     */
	    status = snmp_synch_response( m_agent->getOpaque()->session, pdu, &response);

	    /*
	     * Process the response.
	     */
	    if (status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR) {
	      /*
	       * SUCCESS: Print the result variables
	       */

	      for(vars = response->variables; vars; vars = vars->next_variable)
	        print_variable(vars->name, vars->name_length, vars);

	      /* manipuate the information ourselves */
	      for(vars = response->variables; vars; vars = vars->next_variable) {


	    	  fromSnmpToUaVariant(vars, value, m_type);


	        if (vars->type == ASN_OCTET_STR) {
		  char *sp = (char *)malloc(1 + vars->val_len);
		  memcpy(sp, vars->val.string, vars->val_len);
		  sp[vars->val_len] = '\0';
	          printf("value #%d is a string: %s\n", count++, sp);
		  free(sp);
		}
	        else
	          printf("value #%d is NOT a string! Ack!\n", count++);
	      }
	    } else {
	      /*
	       * FAILURE: print what went wrong!
	       */

	      if (status == STAT_SUCCESS)
	        fprintf(stderr, "Error in packet\nReason: %s\n",
	                snmp_errstring(response->errstat));
	      else if (status == STAT_TIMEOUT)
	        fprintf(stderr, "Timeout: No response from %s.\n",
	        		 m_agent->getOpaque()->session->peername);
	      else
	        snmp_sess_perror("snmpdemoapp",  m_agent->getOpaque()->session);

	    }

	    /*
	     * Clean up:
	     *  1) free the response.
	     *  2) close the session.
	     */
	    if (response)
	      snmp_free_pdu(response);


	    //SOCK_CLEANUP;


    sourceTime = UaDateTime::now();
    return OpcUa_Good;
}



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

void DDataItem::refresh ()
{

	UaVariant value;
    netsnmp_pdu *pdu;
    netsnmp_pdu *response;

    netsnmp_variable_list *vars;
    int status;
    int count=1;


    /*
     * Create the PDU for the data for our request.
     *   1) We're going to GET the system.sysDescr.0 node.
     */
    pdu = snmp_pdu_create(SNMP_MSG_GET);

    std::cout << "len=" << m_internal->oids.size() << std::endl;
    snmp_add_null_var(pdu, &m_internal->oids[0], m_internal->oids.size());

    /*
     * Send the Request out.
     */
    status = snmp_synch_response( m_agent->getOpaque()->session, pdu, &response);

    /*
     * Process the response.
     */
    if (status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR) {
      /*
       * SUCCESS: Print the result variables
       */

      for(vars = response->variables; vars; vars = vars->next_variable)
        print_variable(vars->name, vars->name_length, vars);

      /* manipuate the information ourselves */
      for(vars = response->variables; vars; vars = vars->next_variable) {


    	  fromSnmpToUaVariant(vars, value, m_type);


        if (vars->type == ASN_OCTET_STR) {
	  char *sp = (char *)malloc(1 + vars->val_len);
	  memcpy(sp, vars->val.string, vars->val_len);
	  sp[vars->val_len] = '\0';
          printf("value #%d is a string: %s\n", count++, sp);
	  free(sp);
	}
        else
          printf("value #%d is NOT a string! Ack!\n", count++);
      }
    } else {
      /*
       * FAILURE: print what went wrong!
       */

      if (status == STAT_SUCCESS)
        fprintf(stderr, "Error in packet\nReason: %s\n",
                snmp_errstring(response->errstat));
      else if (status == STAT_TIMEOUT)
        fprintf(stderr, "Timeout: No response from %s.\n",
        		 m_agent->getOpaque()->session->peername);
      else
        snmp_sess_perror("snmpdemoapp",  m_agent->getOpaque()->session);

    }

    /*
     * Clean up:
     *  1) free the response.
     *  2) close the session.
     */
    if (response)
      snmp_free_pdu(response);


    if (m_addressSpaceLink)
    	m_addressSpaceLink->setMonitor(value, OpcUa_Good);

	std::cout << "refresh " << std::endl;
}


}


